import constants from '../constants'

const defaultState = {
  showMessage: false
}

const command = (state = defaultState, action) => {
  switch (action.type) {
    case constants.SEND_COMMAND:
      return { ...state, showMessage: false }
    case constants.SEND_COMMAND_SUCCESS:
      return { ...state, showMessage: true}
    default:
      return state
  }
}

export default command
