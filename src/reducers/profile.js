import constants from '../constants'

const defaultState = {
  isFetching: false,  
  error: '',
  profile: {},
}

const profile = (state = defaultState, action) => {
  switch (action.type) {
    case constants.GET_PROFILE:
      return { ...state, error: null, isFetching: true }
    case constants.GET_PROFILE_FETCHED:      
      return { ...state, isFetching: false, profile: action.payload.profile}
    
    case constants.PUT_USER:
      return { ...state, isFetching: true, updated: false }     
    case constants.PUT_USER_FETCHED:
      return { ...state, isFetching: false, profile: action.payload.user, updated: true } 
    
    case constants.PUT_METER_LABEL:
      return { ...state, isFetching: true, updated: false }     
    case constants.PUT_METER_LABEL_FETCHED:
      return { ...state, isFetching: false, meter: action.payload.meter, updated: true } 

    default:
      return state
  }
}

export default profile
