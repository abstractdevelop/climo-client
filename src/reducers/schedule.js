import constants from '../constants'

const defaultState = {
  isFetching: false,
  error: null,
  schedule: {},
}

const schedule = (state = defaultState, action) => {
  switch (action.type) {
    case constants.GET_SCHEDULE:
      return { ...state, error: null, isFetching: true }
    case constants.GET_SCHEDULE_SUCCESS:      
      return { ...state, isFetching: false, schedule: action.payload.schedule}
    default:
      return state
  }
}

export default schedule
