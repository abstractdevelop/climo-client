import constants from '../constants'

const defaultState = {
  isFetching: false,  
  error: '',
  email: '',
  id: '',
  profile: null,
  changed: false,
}

const user = (state = defaultState, action) => {
  switch (action.type) {
    case constants.USER_FETCHED:
      return { ...state, isFetching: false, ...action.payload.user }    
    case constants.POST_USER:
      return { ...state, isFetching: true }    
    case constants.POST_USER_FETCHED:
        return { ...state, isFetching: false, created: true }    
    case constants.CHANGE_PASSWORD:
        return { ...state, isFetching: true, changed: false }    
    case constants.CHANGE_PASSWORD_FETCHED:        
        return { ...state, isFetching: false, changed: action.payload.data.success }    
    default:
      return state
  }
}

export default user
