import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'

import login from './login'
import user from "./user";
import stats from "./stats";
import profile from './profile'
import meters from './meters';
import homeMeters from './home_meters';
import weathers from './weather';
import home_meters from './home_meters';
import command from './command';
import schedule from './schedule';


const reducers = combineReducers({
  login,
  user,
  stats,
  meters,
  profile,
  homeMeters,
  weathers,
  home_meters,
  command,
  schedule,
  routing: routerReducer
})

export default reducers