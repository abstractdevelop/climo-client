import constants from '../constants'

const defaultState = {
  isFetching: false,  
  error: '',
  meters: null,
  metersSelect: null,
  metersSelected: null
}

const meters = (state = defaultState, action) => {
  switch (action.type) {
    case constants.GET_METERS:
      return { ...state, error: null, isFetching: true }
    case constants.GET_METERS_FETCHED:          
      const metersSelect = action.payload.meters.map( (meter, index )=> { return {key: index, value: meter.deviceId, name: meter.label, on: meter.lastState.pow > 10, id: meter._id} } )
      return { ...state, isFetching: false, meters: action.payload.meters, metersSelect}
    case constants.SET_METER_SELECTED:
      return { ...state, metersSelected: action.payload.meterId }
    default:
      return state
  }
}

export default meters
