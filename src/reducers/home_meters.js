import constants from '../constants'

const defaultState = {
  isFetching: false,  
  error: '',
  temp: 0,
  hum: 0,
  pow: 0,
  brand: '',
  lastControl: {}
}

const home_meters = (state = defaultState, action) => {
  switch (action.type) {
    case constants.GET_METERS_HOME:
      return { ...state, error: null, isFetching: true }
    case constants.GET_METERS_HOME_FETCHED:          
    if(action.payload.data.lastState.time){
      if ((new Date() - new Date(action.payload.data.lastState.time) )/(1000 * 60) < 10 ){
        return {...state,
          temp: action.payload.data.lastState.temp, 
          hum: action.payload.data.lastState.hum, pow: action.payload.data.lastState.pow, 
          brand: action.payload.data.brand,
          lastControl: action.payload.data.lastControl}
      }
      return {...state,
        temp: 0, 
        hum: 0, pow: 0, 
        brand: action.payload.data.brand,
        lastControl: action.payload.data.lastControl}
    }else{
      return { ...state, 
              temp: action.payload.data.lastState.temp, 
              hum: action.payload.data.lastState.hum, pow: action.payload.data.lastState.pow, 
              brand: action.payload.data.brand,
              lastControl: action.payload.data.lastControl}
    }
    default:
      return state
  }
}

export default home_meters
