import constants from '../constants'

const defaultState = {
  isFetching: false,  
  error: '',
  loggedIn: false,
  data: ''
}

const login = (state = defaultState, action) => {
  switch (action.type) {
    case constants.LOGIN:
      return { ...state, error: null, isFetching: true, loggedIn: false }
    case constants.LOGIN_ERROR:
      return { ...state, isFetching: false, error: 'Credenciales inválidas' }
    case constants.LOGGED_IN:      
      return { ...state, isFetching: false, loggedIn: true}
    case constants.LOG_OUT:
      return { ...state, isFetching: false, error: null, token: '' }
    case constants.RECOVER_PASSWORD:
      return {...state, data: '' }
    case constants.RECOVER_PASSWORD_FETCHED:
      return {...state, data: action.payload.data}
    default:
      return state
  }
}

export default login
