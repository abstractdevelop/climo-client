import constants from '../constants'

const defaultState = {
  isFetching: false,  
  error: '',
  stats: null,
}

const stats = (state = defaultState, action) => {
  switch (action.type) {
    case constants.STATS:
      return { ...state, error: null, isFetching: true }
    case constants.STATS_FETCHED:      
      return { ...state, isFetching: false, stats: action.payload.stats}
    default:
      return state
  }
}

export default stats
