import constants from '../constants'

const defaultState = {
  isFetching: false,  
  error: '',
  actualTemperature: 0,
}

const weathers = (state = defaultState, action) => {
  switch (action.type) {
    case constants.GET_CURRENT_WEATHER:
      return { ...state, error: null, isFetching: true }
    case constants.GET_CURRENT_WEATHER_FETCHED:
      return { ...state, actualTemperature: action.payload.currentTemperature}
    default:
      return state
  }
}

export default weathers
