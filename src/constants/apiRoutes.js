let SERVER = process.env.REACT_APP_SERVER_URL

export const METER_MODULE = 'meters'
export const METER_SELECTED = '123'
export const ACTION_MODULE = 'actions';

export const SCHEDULE_MODULE = 'schedules';
export const SCHEDULE_ID = ':scheduleId';
export const METER_ID = ':meterId';

export const DEVICE_ID = '1894';
export const USER_ID = '5a318b2e5926bf76c47e81c1'

export const AUTH_USER_URL = `${SERVER}/auth/oauth/token`

// User
export const USER_URL = `${SERVER}/users`
export const USER_PROFILE_URL = `${SERVER}/users/me`
export const UPDATE_USER_URL = `${SERVER}/users/${USER_ID}`
export const FORGOT_PASS_URL = `${SERVER}/users/forgotPassword ` 
export const CHANGE_PASS_URL = `${SERVER}/users/changePassword ` 
// Devices user (meters)
export const UPDATE_METER_LABEL_URL = `${SERVER}/meters/${METER_ID}`


export const STATS_URL =  `${SERVER}/meters/${DEVICE_ID}/stats?type=all`
export const METERS_URL = `${SERVER}/meters?userId=${USER_ID}&limit=1000`

//METERS
export const METERS_VARIABLES = `${SERVER}/${METER_MODULE}/${METER_SELECTED}`;

//METERS
export const LIST_METERS_URL = `${SERVER}/api/${METER_MODULE}`;

export const SEND_ACTION = `${SERVER}/${ACTION_MODULE}`;

export const LIST_SCHEDULES_URL = `${SERVER}/${SCHEDULE_MODULE}`;
export const SEARCH_SCHEDULE_BY_METER_ID_URL = `${SERVER}/${SCHEDULE_MODULE}/search?deviceId=${METER_ID}`;
export const CREATE_SCHEDULE_URL = `${SERVER}/${SCHEDULE_MODULE}/`;
export const UPDATE_SCHEDULE_URL = `${SERVER}/${SCHEDULE_MODULE}/${SCHEDULE_ID}`;

