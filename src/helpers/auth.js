import Cookies from 'universal-cookie';

const cookie = new Cookies();

const ACCESS_TOKEN_NAME = 'climo_client_access_token';
const REFRESH_TOKEN_NAME = 'climo_client_refresh_token';
const TOKEN_TYPE_NAME = 'climo_client_token_type';
const USER_ROLE_NAME = 'climo_client_user_role';
const USER_ID_NAME = 'climo_client_user_id';

const saveAuth = (auth) => {  
  if (auth) {
    if(auth.refreshToken){
      auth.refresh_token = auth.refreshToken // provisional fix
    }

    if (auth.access_token && auth.refresh_token && auth.token_type) {
      cookie.set(ACCESS_TOKEN_NAME, auth.access_token, { path: '/' });
      cookie.set(REFRESH_TOKEN_NAME, auth.refresh_token, { path: '/' });
      cookie.set(TOKEN_TYPE_NAME, auth.token_type, { path: '/' });
      localStorage.setItem("id", auth._id)
    }
    if (auth._id && auth.role) {
      cookie.set(USER_ROLE_NAME, auth.role, { path: '/' });
      cookie.set(USER_ID_NAME, auth._id, { path: '/' });
    }
    return {
      _id: cookie.get(USER_ID_NAME) || null,
      role: cookie.get(USER_ROLE_NAME) || null,
      access_token: cookie.get(ACCESS_TOKEN_NAME) || null,
      refresh_token: cookie.get(REFRESH_TOKEN_NAME) || null,
      token_type: cookie.get(TOKEN_TYPE_NAME) || null
    };
  } else {
    return null;
  }
};

const loadAuth = () => {
  const _id = cookie.get(USER_ID_NAME);
  const role = cookie.get(USER_ROLE_NAME);
  const access_token = cookie.get(ACCESS_TOKEN_NAME);
  const refresh_token = cookie.get(REFRESH_TOKEN_NAME);
  const token_type = cookie.get(TOKEN_TYPE_NAME);
  if (access_token && refresh_token && token_type) {
    return {
      _id: _id,
      role: role,
      access_token: access_token,
      refresh_token: refresh_token,
      token_type: token_type
    };
  } else {
    return null;
  }
};

const removeAuth = () => {
  cookie.remove(ACCESS_TOKEN_NAME);
  cookie.remove(REFRESH_TOKEN_NAME);
  cookie.remove(TOKEN_TYPE_NAME);
  cookie.remove(USER_ROLE_NAME);
  cookie.remove(USER_ID_NAME);
  localStorage.clear()
  return true;
};

export { saveAuth, loadAuth, removeAuth };
