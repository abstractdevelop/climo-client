import Axios from 'axios';
import { loadAuth, saveAuth } from './auth';
import { AUTH_USER_URL } from '../constants/apiRoutes';
import { CLIENT_ID, CLIENT_SECRET } from '../constants/clientConstants';

function getDefaultHeaders() {
  return {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  };
}

function getHeaders() {
  const auth = loadAuth();
  if (auth) {
    return Object.assign({
      'Authorization': `${auth.token_type} ${auth.access_token}`
    }, getDefaultHeaders());
  }
  return getDefaultHeaders();
}

export const doRequest = (req) => {
  const auth = loadAuth();
  req.headers = getHeaders();
  return new Promise((resolve, reject) => {
    Axios(req).then((response) => {
      resolve(response);
    }).catch((error) => {
      if ((error.message.includes('403') || error.message.includes('401')) && auth && auth.refresh_token) {
        Axios({
          method: 'POST',
          url: AUTH_USER_URL,
          data: {
            grant_type: 'refresh_token',
            refresh_token: auth.refresh_token,
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET
          }
        }).then((response) => {
          saveAuth(response.data);
          req.headers = getHeaders();
          Axios(req).then((res) => {
            resolve(res);
          }).catch((err) => {
            reject(err);
          });
        }).catch((err) => {
          reject(err);
        });
      } else {
        reject(error);
      }
    });
  });
};

export const doMultipleRequest = (reqArr) => {
  let promises = [];
  for(let x = 0;x < reqArr.length;x++)
  {
    const auth = loadAuth();
    reqArr[x].headers = getHeaders();
    promises.push(new Promise((resolve, reject) => {
        Axios(reqArr[x]).then((response) => {
                resolve(response);
            }).catch((error) => {
            if ((error.message.includes('403') || error.message.includes('401')) && auth && auth.refresh_token) {
                Axios({
                method: 'POST',
                url: AUTH_USER_URL,
                data: {
                    grant_type: 'refresh_token',
                    refresh_token: auth.refresh_token,
                    client_id: CLIENT_ID,
                    client_secret: CLIENT_SECRET
                }
                }).then((response) => {
                saveAuth(response.data);
                reqArr[x].headers = getHeaders();
                Axios(reqArr[x]).then((res) => {
                    resolve(res);
                }).catch((err) => {
                    reject(err);
                });
                }).catch((err) => {
                reject(err);
                });
            } else {
                reject(error);
            }
        });
    }));
  }
  return Promise.all(promises).then(response => { 
    return response;
  }).catch(error => { 
    return error;
  });
};
