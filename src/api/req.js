import Axios from 'axios';
import { AUTH_USER_URL } from '../constants/apiRoutes';
import { CLIENT_ID, CLIENT_SECRET } from '../constants/clientConstants';
import { loadAuth, saveAuth } from '../helpers/auth';

function getDefaultHeaders(contentType, accept) {
  return {
    'Content-Type': contentType || 'application/json',
    'Accept': accept || 'application/json'
  };
}

function getHeaders(contentType, accept) {
  const auth = loadAuth();
  if (auth) {
    return Object.assign({
      'Authorization': `${auth.token_type} ${auth.access_token}`
    }, getDefaultHeaders(contentType, accept));
  }
  return getDefaultHeaders(contentType, accept);
}

export const doRequest = (req) => {  
  const auth = loadAuth();
  req.headers = getHeaders();
  return new Promise((resolve, reject) => {
    Axios(req).then((response) => {
      resolve(response);
    }).catch((error) => {
      if ((error.message.includes('403') || error.message.includes('401')) && auth && auth.refresh_token) {
        Axios({
          method: 'POST',
          url: AUTH_USER_URL,
          data: {
            grant_type: 'refresh_token',
            refresh_token: auth.refresh_token,
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET
          }
        }).then((response) => {
          saveAuth(response.data);
          req.headers = getHeaders();
          Axios(req).then((res) => {
            resolve(res);
          }).catch((err) => {
            reject(err);
          });
        }).catch((err) => {
          reject(err);
        });
      } else {
        reject(error);
      }
    });
  });
};

const getBase64 = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.onerror = (error) => {
      reject(error);
    };
  });
};

export const uploadFiles = (req, files) => {
  let error = null;
  return new Promise((resolve, reject) => {
    files.forEach((file, index) => {
      getBase64(file)
        .then((encodedData) => {
          const _req = Object.assign(req, {
            headers: getHeaders(),
            data: {
              lastModified: file.lastModified, // 1446695019000
              name: file.name, // "PRINT THIS - Your two-factor authentication codes.html"
              size: file.size, // 30702
              type: file.type, // "text/html"
              data: encodedData
            }
          });
          Axios(_req).then(() => {
            if (index === files.length - 1 && !error) {
              resolve(true);
            } else if (error) {
              reject(error);
            }
          }).catch((err) => {
            if (index === files.length - 1) {
              reject(err);
            } else {
              error = err;
            }
          });
        })
        .catch((err) => {
          if (index === files.length - 1) {
            reject(err);
          } else {
            error = err;
          }
        }); // prints the base64 string
    });
  });
};
