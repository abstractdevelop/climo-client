import Axios from 'axios';

export const currentWeather = (lat, lng) => {
  return new Promise((resolve, reject) => {
    Axios({
        method: 'POST',
        url: `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&appid=889e5295ac7b291a7b7415f9800bd5c8&units=metric`,
      }).then((result) => {
        resolve(result)
      })
  })
}