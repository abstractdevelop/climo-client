import constants from '../constants/'

export const fetchMetersForVariables = (meterId) => {
  return {
    type: constants.GET_METERS_HOME,
    payload: {
      meterId
    }
  }
}

export const fetchMetersForVariablesFetched = (data) => {
  return {
    type: constants.GET_METERS_HOME_FETCHED,
    payload: {
      data
    }
  }
}



export const currentWeather = (coords) => {
  return {
    type: constants.GET_CURRENT_WEATHER,
    payload: {
      lat: coords.latitude,
      lng: coords.longitude,
    }
  }
}

export const currentWeatherFetched = (result) => {
  return {
    type: constants.GET_CURRENT_WEATHER_FETCHED,
    payload: {
      currentTemperature: result.main.temp
    }
  }
}