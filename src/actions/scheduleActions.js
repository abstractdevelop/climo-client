import constants from '../constants/'

export const getSchedule = (deviceId) => {
  return {
    type: constants.GET_SCHEDULE,
    payload: {
        deviceId
    }
  }
}

export const getScheduleSuccess = (schedule) => {
  return {
    type: constants.GET_SCHEDULE_SUCCESS,
    payload:{
        schedule
    }
  }
}

//update schedule
export const updateSchedule = (schedule) => {
  return {
    type: constants.UPDATE_SCHEDULE,
    payload: {
      schedule
    }
  };
};

export const updateScheduleSuccess = (schedule) => {
  return {
    type: constants.UPDATE_SCHEDULE_SUCCESS,
    payload: {
      schedule
    }
  };
};




