import constants from '../constants/'

export const userFetched = (user) => {
  return {
    type: constants.USER_FETCHED,
    payload:{
      user: user
    }
  }
}

export const postUser = (user) => {
  return {
    type: constants.POST_USER,
    payload:{
      user: user
    }
  }
}

export const postUserFetched = (user) => {
  return {
    type: constants.POST_USER_FETCHED,
    payload:{
      user: user
    }
  }
}

export const postChangePassword = (password, token) => {
  return {
    type: constants.CHANGE_PASSWORD,
    payload:{
      password, token
    }
  }
}

export const postChangePasswordFetched = (data) => {
  return {
    type: constants.CHANGE_PASSWORD_FETCHED,
    payload:{
      data
    }
  }
}