import constants from '../constants/'

export const getProfile = () => {
  return {
    type: constants.GET_PROFILE,
  }
}

export const getProfileFetched = (profile) => {
  return {
    type: constants.GET_PROFILE_FETCHED,
    payload:{
      profile
    }
  }
}
// Update user profile  
export const putUser = (user) => {
  return {
    type: constants.PUT_USER,
    payload: {
      user: user
    }
  }
}

export const putUserFetched = (user) => {
  return {
    type: constants.PUT_USER_FETCHED,
    payload: {
      user: user
    }
  }
}

// Update meter label user
export const putMeterLabel = (meter) => {
  return {
    type: constants.PUT_METER_LABEL, 
    payload: {
      meter: meter
    }
  }
}

export const putMeterLabelFetched = (meter) => {
  return {
    type: constants.PUT_METER_LABEL_FETCHED,
    payload: {
      meter: meter
    }
  }
}


