import constants from '../constants/'

export const getStats = (deviceId) => {
  return {
    type: constants.STATS,
    payload:{
      deviceId
    }
  }
}

export const getStatsFetched = (stats) => {
  return {
    type: constants.STATS_FETCHED,
    payload:{
      stats
    }
  }
}
