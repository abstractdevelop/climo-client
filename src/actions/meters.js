import constants from '../constants/'

export const getMeters = (userId) => {
  return {
    type: constants.GET_METERS,
    payload:{
      userId
    }
  }
}

export const getMetersFetched = (meters) => {
  return {
    type: constants.GET_METERS_FETCHED,
    payload:{
      meters
    }
  }
}

export const setMeterSelected = (meterId) => {
  return {
    type: constants.SET_METER_SELECTED,
    payload:{
      meterId
    }
  }
}