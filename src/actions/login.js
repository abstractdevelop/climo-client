import constants from '../constants/'

export const attemptLogIn = (user) => {
  return {
    type: constants.LOGIN,
    payload:{
      user: user
    }
  }
}

export const logInError = (error) => {
  return {
    type: constants.LOGIN_ERROR,
    payload:{
      error: error
    }
  }
}

export const loggedIn = (userData) => {
  return {
    type: constants.LOGGED_IN,
    payload:{
      userData,
    }
  }
}

export const logOut = () => {
  return {
    type: constants.LOG_OUT,
  }
}

export const recoverPassword = (email) => {
  return {
    type: constants.RECOVER_PASSWORD,
    payload: {
      email
    }
  }
}

export const recoverPasswordFetched = (data) => {
  return {
    type: constants.RECOVER_PASSWORD_FETCHED,
    payload: {
      data
    }
  }
}
