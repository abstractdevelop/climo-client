import * as authHelper from '../helpers/auth';
import * as actionTypes from '../constants/actionTypes';

const FORBIDDEN = 'code 403';
const UNAUTHORIZED = 'code 401';

export const setError = (error) => {
  let _error;
  if (error.message.includes(FORBIDDEN)) {
    authHelper.removeAuth();
    _error = new Error('Email o contraseña incorrectos');
  } else if (error.message.includes(UNAUTHORIZED)) {
    authHelper.removeAuth();
    _error = new Error('Debe autenticarse antes de realizar esta acción');
  } else {
    _error = error;
  }
  return {
    type: actionTypes.SET_ERROR,
    error: _error
  };
};

export const clearError = () => {
  return {
    type: actionTypes.CLEAR_ERROR
  };
};
