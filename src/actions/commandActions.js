import * as actionTypes from '../constants/actionTypes';

export const sendCommandSuccess = (command) => {
  return {
    type: actionTypes.SEND_COMMAND_SUCCESS,
    payload: {
      command
    }
  };
};


export const sendCommand = (command) => {
  return {
    type: actionTypes.SEND_COMMAND,
    payload: {
      command
    }
  }
};
