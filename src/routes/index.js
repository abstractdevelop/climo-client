import React from 'react';
  

import { BrowserRouter, Route } from 'react-router-dom';
import Login from '../containers/login'
import Home from '../containers/home';
import User from '../containers/profile'
import Stats from '../containers/stats'
import Navbar from '../components/navbar'
import Register from '../containers/register'
import RecoverPass from '../containers/recoverPass'
import ChangePass from '../containers/changePass'

function App() {
  return (
    <div className="App" > 
        <BrowserRouter>
          <div>
            <Navbar />
            <Route exact path="/login" component={Login}/>
            <Route exact path="/users" component={User}/>
            <Route exact path="/" component={Home}/>
            <Route exact path="/stats" component={Stats}/>
            <Route exact path="/register" component={Register}/>
            <Route exact path="/recover" component={RecoverPass}/>
            <Route exact path="/change/:resetPassword" component={ChangePass}/>
          </div>
        </BrowserRouter>
    </div>
  );
}

export default App;
