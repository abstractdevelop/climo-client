import { connect } from 'react-redux'
import RecoverPassScreen from '../screens/recoverPass';
import { recoverPassword } from '../actions/login';


const mapStateToProps = ({login }) => {
    return {...login}
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      recoverPassword: (email) => {
        dispatch(recoverPassword(email))
      }
    }
  }
  
const Login = connect(mapStateToProps,mapDispatchToProps)(RecoverPassScreen)

export default Login