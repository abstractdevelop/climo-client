import { connect } from 'react-redux'
import HomeScreen from '../screens/home';
import { fetchMetersForVariables, currentWeather } from '../actions/home';
import { getSchedule, updateSchedule } from '../actions/scheduleActions';
import { sendCommand } from '../actions/commandActions';
import { getMeters } from '../actions/meters'
import * as authHelper from '../helpers/auth';


const mapStateToProps = ({weathers, meters, home_meters, command, schedule}) => {
    return {
        auth: authHelper.loadAuth(),
        actualTemperature: weathers.actualTemperature,
        ...meters,
        ...home_meters,
        ...command,
        ...schedule
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        fetchMetersForVariables: (meterId) => {
            dispatch(fetchMetersForVariables(meterId))
        },
        currentWeather: (latitude, longitude) => {
          dispatch(currentWeather({latitude, longitude}))
        },
        getMeters: (userId) =>{
            dispatch(getMeters(userId))
        },
        sendCommandExec: (command) => {
          dispatch(sendCommand(command))
        },
        getSchedule: (deviceId) => {
          dispatch(getSchedule(deviceId))
        },
        updateSchedule: (schedule) => {
          dispatch(updateSchedule(schedule))
        }
    }
  }
  
const Login = connect(mapStateToProps,mapDispatchToProps)(HomeScreen)

export default Login