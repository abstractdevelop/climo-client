import { connect } from 'react-redux'
import LoginScreen from '../screens/login';
import { attemptLogIn } from '../actions/login';


const mapStateToProps = ({login }) => {
    return {...login}
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      attemptLogIn: (user) => {
        dispatch(attemptLogIn(user))
      }
    }
  }
  
const Login = connect(mapStateToProps,mapDispatchToProps)(LoginScreen)

export default Login