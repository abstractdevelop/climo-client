import { connect } from 'react-redux'
import RegisterScreen from '../screens/register';
import { postUser } from '../actions/user';


const mapStateToProps = ({user }) => {
    return {...user}
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      postUser: (user) => {
        dispatch(postUser(user))
      }
    }
  }
  
const Register = connect(mapStateToProps,mapDispatchToProps)(RegisterScreen)

export default Register