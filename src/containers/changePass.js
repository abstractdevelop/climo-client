import { connect } from 'react-redux'
import ChangePassScreen from '../screens/changePass';
import { postChangePassword } from '../actions/user';


const mapStateToProps = ({user }) => {
    return {...user}
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      postChangePassword: (password, token) => {
        dispatch(postChangePassword(password, token))
      }
    }
  }
  
const Login = connect(mapStateToProps,mapDispatchToProps)(ChangePassScreen)

export default Login