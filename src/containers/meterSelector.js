import { connect } from 'react-redux'
import MetersSelectorComponent from '../components/meterSelector';
import { getMeters, setMeterSelected } from '../actions/meters'


const mapStateToProps = ({meters }) => {
    return {...meters}
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      setMeterSelected: (meterId) => {
        dispatch(setMeterSelected(meterId))
      },
      getMeters: (userId) =>{
        dispatch(getMeters(userId))
      },
    }
  }
  
const MeterSelector = connect(mapStateToProps,mapDispatchToProps)(MetersSelectorComponent)

export default MeterSelector