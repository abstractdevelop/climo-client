import { connect } from 'react-redux'
import StatsScreen from '../screens/stats';
import { getStats } from '../actions/stats';
import { getMeters } from '../actions/meters'


const mapStateToProps = ({stats, user, meters }) => {
    return {...stats, ...user, ...meters}
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      getStats: (deviceId) => {
        dispatch(getStats(deviceId))
      },
      getMeters: (userId) =>{
        dispatch(getMeters(userId))
      },
    }
  }
  
const Login = connect(mapStateToProps,mapDispatchToProps)(StatsScreen)

export default Login