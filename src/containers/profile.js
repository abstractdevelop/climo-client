import { connect } from 'react-redux'
import UserScreen from '../screens/users'
import { getProfile, putUser, putMeterLabel } from '../actions/profile'
import { getMeters } from '../actions/meters'


const mapStateToProps = ({ profile, meters }) => {
    return {...profile, ...meters }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      getProfile: () =>{
        dispatch(getProfile())
      },
      putUser: (user) => {
        dispatch(putUser(user))
      },
      getMeters: (userId) => {
        dispatch(getMeters(userId))
      },
      putMeterLabel: (meterId) => {
        dispatch(putMeterLabel(meterId))
      }
    }
  }
  
const UserProfile = connect(mapStateToProps,mapDispatchToProps)(UserScreen)

export default UserProfile