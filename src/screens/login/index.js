import React, { Component } from 'react'
import { FormControl, InputLabel, Input, Container, InputAdornment, IconButton, Button} from '@material-ui/core';
import {EmailOutlined, VisibilityOff, Visibility, Lock} from '@material-ui/icons'
import { pink } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/core/styles';
import * as clientConstants from '../../constants/clientConstants';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../index.scss'


const ColorButton = withStyles(theme => ({
  root: {
    color: theme.palette.getContrastText(pink[400]),
    backgroundColor: pink[400],
    '&:hover': {
      backgroundColor: pink[500],
    },
  },
}))(Button);


export default class Login extends Component {
  constructor(props) {
    super(props)

    this.state = {
      username: '',
      password: '',
      weight: '',
      weightRange: '',
      showPassword: false,
    }
  }

  componentDidUpdate(prevProps, prevState){
    if(this.props.loggedIn && this.props.loggedIn !== prevProps.loggedIn){
      this.props.history.push("/")
    }
    if(this.props.error && this.props.error !== prevProps.error){
      toast.error(this.props.error )
    }
  }

  handleChange = prop => event => {
    this.setState({  [prop]: event.target.value });
  };

  handleClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };

  handleSubmit = () => {
    const {username, password} = this.state    
    const user = { username: username, password: password, 
      grant_type: clientConstants.GRANT_TYPE, client_id: clientConstants.CLIENT_ID, 
      client_secret: clientConstants.CLIENT_SECRET }
    this.props.attemptLogIn(user)
  }

  render() {
    return (              
      <Container component="form" maxWidth="sm" style={{backgroundColor: "white", padding: "1rem"}}>
        <ToastContainer />
        <div className="form-container"> 
          <div style={{fontSize: "1.5rem", margin: "1rem 0" }}>Bienvenido a Climo</div>
                     
            <FormControl>
              <InputLabel htmlFor="my-input">Email</InputLabel>              
              <Input id="my-input" aria-describedby="my-helper-text"
              value={this.state.username}
              onChange={this.handleChange('username')}
              autoComplete="username email"
               startAdornment={
                <InputAdornment position="start">
                   <EmailOutlined></EmailOutlined>
                </InputAdornment>
               }/>            
            </FormControl>  
          
            <FormControl >
                <InputLabel htmlFor="adornment-password">Contraseña</InputLabel>
                <Input
                  id="adornment-password"
                  type={this.state.showPassword ? 'text' : 'password'}
                  value={this.state.password}
                  onChange={this.handleChange('password')}
                  autoComplete="current-password"
                  startAdornment={
                    <InputAdornment position="start">
                      <Lock></Lock>
                    </InputAdornment>
                  }
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton aria-label="Toggle password visibility" onClick={this.handleClickShowPassword}>
                        {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>        
              <div style={{margin: "1rem 0", display: "flex", flexDirection: "row-reverse", justifyContent: "space-between"}}>
                <ColorButton onClick={this.handleSubmit} variant="contained">Ingresar</ColorButton>  
                <Button style={{color: "gray", fontSize: "0.7rem"}} onClick={()=> this.props.history.push("/recover")} >¿Olvidó su contraseña?</Button>
              </div>
              

        </div>                    
      </Container>
    )
  }
}
