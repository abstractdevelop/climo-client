import React, { Component } from 'react'
import Chart from '../../components/chart'
import { Button, Container } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import MeterSelector from '../../containers/meterSelector'
import './index.scss'
import 'font-awesome/css/font-awesome.css'
import CircularProgress from '@material-ui/core/CircularProgress';


export default class Stats extends Component {
  constructor(props) {
    super(props);

    this.state = {
      metersSelected: "",
      series: {},
      serie: [],
      title: "",     
      yAxisLabelFormat: "{value}",
      tooltip: {
        valueDecimals: 2,        
        valueSuffix: ''
      },
      displayMessage: false,
    }
    this.setSerie = this.setSerie.bind(this)
    this.timer = setTimeout(this.enableMessage.bind(this), 2000);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  enableMessage() {
    this.setState({displayMessage: true});
  }


  componentDidMount(){        
    const metersSelected = localStorage.getItem("meterSelected")
    if (metersSelected){
      this.setState({metersSelected})      
    }
    
  }

  componentDidUpdate(prevProps, prevState){
    if(this.props.metersSelected && prevProps.metersSelected !== this.props.metersSelected && this.props.metersSelected !== this.state.metersSelected ){      
      this.setState({metersSelected: this.props.metersSelected})
    }
    if(this.state.metersSelected !== prevState.metersSelected){
      this.props.getStats(this.state.metersSelected )
    }
    if(this.props.stats && this.props.stats !== prevProps.stats){
      this.setState({
        series: this.props.stats,
      });      
    }  
    if(this.state.series && this.state.series !== prevState.series){
      this.setSerie("temperature")
    }
  }

  checkVariable(variable){
    if (variable !== this.state.title){
      return ""
    }
    switch (this.state.title) {
      case "Temperatura":
        return "active--temperature"        
      case "Humedad":
        return "active--humidity"
      case "Energía":
        return "active--energy"
      default:
        return ""
    }
  }

  setSerie(type){
    if(this.state.series){
      if(type === "temperature"){
        this.setState({ 
          serie: { name: "Temperatura", data: this.props.stats.tf, type: "line", color: '#BF0B23', 
            dataGrouping: {
              enabled: true,  
              approximation: 'average',            
              units: [
                  ['hour', [1]],
                  ['day', [1]],
                  ['month', [1]],
                  ['year', null]
              ],
              groupPixelWidth: 100                         
            }
          }, 
          title: "Temperatura",
          yAxisLabelFormat: "{value} C°",
          tooltip: {
            valueDecimals: 2,        
            valueSuffix: ' C°'
          },
        })
      }else if(type === "humidity"){
        this.setState({ 
          serie: { name: "Humedad", data: this.props.stats.hf, type: "line", color: "#009FE9",
          dataGrouping: {
            enabled: true,  
            approximation: 'average',            
            units: [
                ['hour', [1]],
                ['day', [1]],
                ['month', [1]],
                ['year', null]
            ],
            groupPixelWidth: 100                         
          }
        }, 
          title: "Humedad",
          yAxisLabelFormat: "{value} %",
          tooltip: {
            valueDecimals: 2,        
            valueSuffix: ' %'
          },
        })
      }else{
        this.setState({ 
          serie: { name: "Energía", data: this.props.stats.e, type: "column", color: "#B1D12A",
            dataGrouping: {
              enabled: true,  
              approximation: 'sum',            
              units: [
                  ['hour', [1]],
                  ['day', [1]],
                  ['month', [1]],
                  ['year', null]
              ],
              groupPixelWidth: 100                         
            }
          }, 
          title: "Energía",  
          yAxisLabelFormat: "{value} Wh" ,
          tooltip: {
            valueDecimals: 0,        
            valueSuffix: ' Wh'
          },      
        })
      }
    }
  }

  render() {
    return (
      <div style={{display: "flex", flexDirection: "column"}}> 
        <div style={{display: "flex", justifyContent: "center"}}>
          <MeterSelector ></MeterSelector>
        </div>      
                
        { this.props.stats && Object.keys(this.props.stats).length > 0 ?
          <Container style={{ display: `${ this.state.metersSelected.length === 0 ? "none" : ""  }` }} maxWidth="lg">
            <div  style={{ display: "flex", margin: "2rem" }}>
              <div className="stats-button__container">
                <Button className={`button-stats--temperature ${this.checkVariable("Temperatura")}`}   onClick={() => this.setSerie("temperature")}><span>Temperatura</span>   <Icon className='fa fa-thermometer-half'>  </Icon></Button> 
                <Button className={`button-stats--humidity ${this.checkVariable("Humedad")}`} onClick={() => this.setSerie("humidity")}><span> Humedad</span>   <Icon className="fa fa-tint"></Icon> </Button> 
                <Button className={`button-stats--energy ${this.checkVariable("Energía")}`} onClick={() => this.setSerie("energy")}><span> Energia Consumida</span>   <Icon className="fa fa-bolt"></Icon> </Button> 
              </div>
              <Chart series={this.state.serie}  title={this.state.title} yAxisLabelFormat={this.state.yAxisLabelFormat} tooltip={this.state.tooltip}></Chart>
            </div>
          </Container>
          :
          <Container maxWidth="lg" style={{display: "flex", justifyContent: "center"}}>            
            { this.state.displayMessage ? <h1>Sin Datos</h1> : <CircularProgress></CircularProgress> }
          </Container>
        }
        
        
      </div>
    )
  }
}
