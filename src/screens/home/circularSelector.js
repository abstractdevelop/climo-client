import React from 'react';
import Knob from '../../components/Knob';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

const circularSelector = (props) => {
    return(
        <div>
            <div>
                <Knob value={props.activeTemp}
                    font="sans-serif"
                    thickness={0.2}
                    readOnly={true}
                    min={12}
                    max={36}
                    bgColor="#FFF"
                    width={300}
                    height={300}
                    disableTextInput={true}
                    fgColor={props.getColor()}
                    inputColor={props.getColor()}
                    fontWeight="400" onChange={temp => {
                    if (temp >= 18 && temp <= 30) {
                        console.log('holi')
                    }
                }}/>
                <span style={{
                    display: window.innerWidth <= 992 || window.innerWidth >= 1200 ? 'inline' : 'none',
                    float: 'left',
                    color: props.getColor(),
                    fontWeight: 500,
                    fontSize: 50,
                    position:'absolute',
                    marginTop: 100,
                    marginLeft: -100
                }}>º</span>
            </div>
            <div className="up-low-temp">
                <Fab size="small" color="secondary" aria-label="Add" onClick={event => {
                      event.preventDefault();
                      if (props.isOn && !props.fan) {
                        props.handleChangeTemp(props.activeTemp - 1);
                      }
                    }}>
                    <RemoveIcon />
                </Fab>
                <Fab size="small" color="secondary" aria-label="Add" onClick={event => {
                      event.preventDefault();
                      if (props.isOn && !props.fan) {
                        props.handleChangeTemp(props.activeTemp + 1);
                      }
                    }}>
                    <AddIcon />
                </Fab>
            </div>
        </div>
    )
}

export default circularSelector