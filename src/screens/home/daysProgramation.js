import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Tooltip from '@material-ui/core/Tooltip';
import Fab from '@material-ui/core/Fab';
import Toys from '@material-ui/icons/Toys';
import BrightnessLow from '@material-ui/icons/BrightnessLow';
import AcUnit from '@material-ui/icons/AcUnit';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';

export default function DaysProgramation(props) { 
    return(
        <div>
            <FormControlLabel control={<Checkbox value="checkedC" checked={props.schedule.isActive} onChange={(e) => props.toggleCheck(e, props.schedulePosition)}/>} label={props.schedule.id} />
            <small style={{color: '#b1d12a', fontWeight: 'bold'}}>ON</small>
            <div className="buttons-schedule">
                <Fab className={props.isActive ? 'button-schedule' : ''} disabled={!props.isActive} size="small" color="secondary" aria-label="Add" onClick={event => {
                      event.preventDefault();
                      if (props.isActive) {
                        props.handleTemp(props.schedulePosition, 'baja');
                      }
                    }}>
                    <RemoveIcon />
                </Fab>
                <span>&nbsp;&nbsp;{props.temp}º&nbsp;</span>
                <Fab className={props.isActive ? 'button-schedule' : ''} disabled={!props.isActive} size="small" color="secondary" aria-label="Add" onClick={event => {
                      event.preventDefault();
                      if (props.isActive) {
                        props.handleTemp(props.schedulePosition, 'sube');
                      }
                    }}>
                    <AddIcon />
                </Fab>
            </div>
            <div style={{display: 'flex', flexDirection: 'row', margin: '15px 0 15px 0'}}>
                <Tooltip title="Ventilador" aria-label="Add" placement="top">
                    <div>
                        <Fab disabled={!props.schedule.isActive} size="small" aria-label="Add" onClick={() => {
                            if(props.schedule.isActive) {
                                props.handleControlSchedule(props.index,'fan')
                            }
                        }}>
                            <Toys className={props.schedule.fan === "fanOff" ? '' : 'schedule-fan'}/>
                        </Fab>
                    </div>
                </Tooltip>
                <Tooltip title="Calor" aria-label="Add" placement="top">
                    <div>
                        <Fab disabled={!props.schedule.isActive} size="small" aria-label="Add" onClick={() => {
                            if(props.schedule.isActive) {
                                props.handleControlSchedule(props.index,'hot')
                            }
                        }}>
                            <BrightnessLow className={props.schedule.hot === "hotOff" ? '' : 'schedule-hot'}/>
                        </Fab>
                    </div>
                </Tooltip>
                <Tooltip title="Frío" aria-label="Add" placement="top">
                    <div>
                        <Fab disabled={!props.schedule.isActive} size="small" aria-label="Add" onClick={() => {
                            if(props.schedule.isActive) {
                                props.handleControlSchedule(props.index,'cold')
                            }
                        }}>
                            <AcUnit className={props.schedule.cold === "coldOff" ? '' : 'schedule-cold'}/>
                        </Fab>
                    </div>
                </Tooltip>
            </div>
            <FormControl>
                <div>
                    <Select disabled={!props.schedule.isActive} value={props.turnOn || ''} onChange={(e) => props.handleTimeChangeOn(e,props.index)}>
                        <MenuItem value='0'>12:00 AM</MenuItem>
                        <MenuItem value='1800'>12:30 AM</MenuItem>
                        <MenuItem value='3600'>01:00 AM</MenuItem>
                        <MenuItem value='5400'>01:30 AM</MenuItem>
                        <MenuItem value='7200'>02:00 AM</MenuItem>
                        <MenuItem value='9000'>02:30 AM</MenuItem>
                        <MenuItem value='10800'>03:00 AM</MenuItem>
                        <MenuItem value='12600'>03:30 AM</MenuItem>
                        <MenuItem value='14400'>04:00 AM</MenuItem>
                        <MenuItem value='16200'>04:30 AM</MenuItem>
                        <MenuItem value='18000'>05:00 AM</MenuItem>
                        <MenuItem value='19800'>05:30 AM</MenuItem>
                        <MenuItem value='21600'>06:00 AM</MenuItem>
                        <MenuItem value='23400'>06:30 AM</MenuItem>
                        <MenuItem value='25200'>07:00 AM</MenuItem>
                        <MenuItem value='27000'>07:30 AM</MenuItem>
                        <MenuItem value='28800'>08:00 AM</MenuItem>
                        <MenuItem value='30600'>08:30 AM</MenuItem>
                        <MenuItem value='32400'>09:00 AM</MenuItem>
                        <MenuItem value='34200'>09:30 AM</MenuItem>
                        <MenuItem value='36000'>10:00 AM</MenuItem>
                        <MenuItem value='37800'>10:30 AM</MenuItem>
                        <MenuItem value='39600'>11:00 AM</MenuItem>
                        <MenuItem value='41400'>11:30 AM</MenuItem>
                        <MenuItem value='43200'>12:00 PM</MenuItem>
                        <MenuItem value='45000'>12:30 PM</MenuItem>
                        <MenuItem value='46800'>01:00 PM</MenuItem>
                        <MenuItem value='48600'>01:30 PM</MenuItem>
                        <MenuItem value='50400'>02:00 PM</MenuItem>
                        <MenuItem value='52200'>02:30 PM</MenuItem>
                        <MenuItem value='54000'>03:00 PM</MenuItem>
                        <MenuItem value='55800'>03:30 PM</MenuItem>
                        <MenuItem value='57600'>04:00 PM</MenuItem>
                        <MenuItem value='59400'>04:30 PM</MenuItem>
                        <MenuItem value='61200'>05:00 PM</MenuItem>
                        <MenuItem value='63000'>05:30 PM</MenuItem>
                        <MenuItem value='64800'>06:00 PM</MenuItem>
                        <MenuItem value='66600'>06:30 PM</MenuItem>
                        <MenuItem value='68400'>07:00 PM</MenuItem>
                        <MenuItem value='70200'>07:30 PM</MenuItem>
                        <MenuItem value='72000'>08:00 PM</MenuItem>
                        <MenuItem value='73800'>08:30 PM</MenuItem>
                        <MenuItem value='75600'>09:00 PM</MenuItem>
                        <MenuItem value='77400'>09:30 PM</MenuItem>
                        <MenuItem value='79200'>10:00 PM</MenuItem>
                        <MenuItem value='81000'>10:30 PM</MenuItem>
                        <MenuItem value='82800'>11:00 PM</MenuItem>
                        <MenuItem value='84600'>11:30 PM</MenuItem>
                    </Select>
                </div>
            </FormControl>
            <small style={{color: '#ff1b49', fontWeight: 'bold', marginTop: '30px'}}>OFF</small>
            <FormControl>
                <div>
                    <Select disabled={!props.schedule.isActive} value={props.turnOff || ''} onChange={(e) => props.handleTimeChangeOff(e,props.index)} inputProps={{name: 'endTime',id: 'end-time',}}>
                        <MenuItem value='0'>12:00 AM</MenuItem>
                        <MenuItem value='1800'>12:30 AM</MenuItem>
                        <MenuItem value='3600'>01:00 AM</MenuItem>
                        <MenuItem value='5400'>01:30 AM</MenuItem>
                        <MenuItem value='7200'>02:00 AM</MenuItem>
                        <MenuItem value='9000'>02:30 AM</MenuItem>
                        <MenuItem value='10800'>03:00 AM</MenuItem>
                        <MenuItem value='12600'>03:30 AM</MenuItem>
                        <MenuItem value='14400'>04:00 AM</MenuItem>
                        <MenuItem value='16200'>04:30 AM</MenuItem>
                        <MenuItem value='18000'>05:00 AM</MenuItem>
                        <MenuItem value='19800'>05:30 AM</MenuItem>
                        <MenuItem value='21600'>06:00 AM</MenuItem>
                        <MenuItem value='23400'>06:30 AM</MenuItem>
                        <MenuItem value='25200'>07:00 AM</MenuItem>
                        <MenuItem value='27000'>07:30 AM</MenuItem>
                        <MenuItem value='28800'>08:00 AM</MenuItem>
                        <MenuItem value='30600'>08:30 AM</MenuItem>
                        <MenuItem value='32400'>09:00 AM</MenuItem>
                        <MenuItem value='34200'>09:30 AM</MenuItem>
                        <MenuItem value='36000'>10:00 AM</MenuItem>
                        <MenuItem value='37800'>10:30 AM</MenuItem>
                        <MenuItem value='39600'>11:00 AM</MenuItem>
                        <MenuItem value='41400'>11:30 AM</MenuItem>
                        <MenuItem value='43200'>12:00 PM</MenuItem>
                        <MenuItem value='45000'>12:30 PM</MenuItem>
                        <MenuItem value='46800'>01:00 PM</MenuItem>
                        <MenuItem value='48600'>01:30 PM</MenuItem>
                        <MenuItem value='50400'>02:00 PM</MenuItem>
                        <MenuItem value='52200'>02:30 PM</MenuItem>
                        <MenuItem value='54000'>03:00 PM</MenuItem>
                        <MenuItem value='55800'>03:30 PM</MenuItem>
                        <MenuItem value='57600'>04:00 PM</MenuItem>
                        <MenuItem value='59400'>04:30 PM</MenuItem>
                        <MenuItem value='61200'>05:00 PM</MenuItem>
                        <MenuItem value='63000'>05:30 PM</MenuItem>
                        <MenuItem value='64800'>06:00 PM</MenuItem>
                        <MenuItem value='66600'>06:30 PM</MenuItem>
                        <MenuItem value='68400'>07:00 PM</MenuItem>
                        <MenuItem value='70200'>07:30 PM</MenuItem>
                        <MenuItem value='72000'>08:00 PM</MenuItem>
                        <MenuItem value='73800'>08:30 PM</MenuItem>
                        <MenuItem value='75600'>09:00 PM</MenuItem>
                        <MenuItem value='77400'>09:30 PM</MenuItem>
                        <MenuItem value='79200'>10:00 PM</MenuItem>
                        <MenuItem value='81000'>10:30 PM</MenuItem>
                        <MenuItem value='82800'>11:00 PM</MenuItem>
                        <MenuItem value='84600'>11:30 PM</MenuItem>
                    </Select>
                </div>
            </FormControl>
        </div>
    )
}


