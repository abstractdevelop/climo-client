import React, { Component } from 'react';
import Container from '@material-ui/core/Container';
import PowerSettingsNew from '@material-ui/icons/PowerSettingsNew';
import Toys from '@material-ui/icons/Toys';
import BrightnessLow from '@material-ui/icons/BrightnessLow';
import AcUnit from '@material-ui/icons/AcUnit';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import {withRouter} from 'react-router-dom'
import CurrentVariables from './currentVariables';
import MeterSelector from '../../containers/meterSelector'
import DaysProgramation from './daysProgramation';
import CircularSelector from './circularSelector';
import * as commands from '../../constants/commands';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './index.scss';

let commandInterval;
class Home extends Component {
    commandSended = () => toast.success("Comando enviado !");

    constructor(props) {
        super(props)
        this.updateSchedule = {mon:{prog:{}},tue:{prog:{}},wed:{prog:{}},thu:{prog:{}},fri:{prog:{}},sat:{prog:{}},sun:{prog:{}}};
        this.state = {
            age: 'default',
            latitude: '',
            longitude: '',
            daysWeek: ['Lunes','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo'],
            active: true,
            isOn: false,
            isHandle: false,
            fan: false,
            hot: false,
            cold: false,
            activeTemp: 22,
            activeCommand: {},
            activeMeter: {},
            max: 30,
            min: 18,
            checkSchedule:[]
        }
    }

    componentDidMount() {
       
      this.handleGeoReference();
      // this.props.fetchMetersForVariables(localStorage.getItem('meterId'))
      // this.props.getSchedule(localStorage.getItem('meterSelected'))
      if(localStorage.getItem("meterOn") === "true" ){
        this.setState({isOn: true}) 
      }
      if(Object.keys(this.props.schedule).length > 0){
        const loadSchedule = [];

        loadSchedule[0] = this.props.schedule.mon.prog;
        loadSchedule[0].id = 'Lunes';
        loadSchedule[1] = this.props.schedule.tue.prog;
        loadSchedule[1].id = 'Martes';
        loadSchedule[2] = this.props.schedule.wed.prog;
        loadSchedule[2].id = 'Miercoles';
        loadSchedule[3] = this.props.schedule.thu.prog;
        loadSchedule[3].id = 'Jueves';
        loadSchedule[4] = this.props.schedule.fri.prog;
        loadSchedule[4].id = 'Viernes';
        loadSchedule[5] = this.props.schedule.sat.prog;
        loadSchedule[5].id = 'Sabado';
        loadSchedule[6] = this.props.schedule.sun.prog;
        loadSchedule[6].id = 'Domingo';

        for(let i=0;i<7;i++){
          switch (loadSchedule[i].modo) {
            case 'fan':
              loadSchedule[i].fan='fanOn';
              loadSchedule[i].cold='coldOff';
              loadSchedule[i].hot='hotOff';
            break;
            case 'cold':
              loadSchedule[i].fan='fanOff';
              loadSchedule[i].cold='coldOn';
              loadSchedule[i].hot='hotOff';
            break;
            case 'hot':
              loadSchedule[i].fan='fanOff';
              loadSchedule[i].cold='coldOff';
              loadSchedule[i].hot='hotOn';
            break;
            default:
          }
        }

        this.setState({checkSchedule:loadSchedule});
      }
    }

    commandCircularSelector = () => {
      this.sendCommand(null);
    }

    componentDidUpdate(prevProps, prevState) {
        //Pregunta si tiene latitud y longitud para mostrar el clima actual en el <Paper/>
        if(this.state.latitude && this.state.latitude !== prevState.latitude && this.state.longitude && this.state.longitude !== prevState.longitude) {
            this.props.currentWeather(this.state.latitude, this.state.longitude)
        }

        if(this.props.metersSelected && prevProps.metersSelected !== this.props.metersSelected ){  
          this.props.fetchMetersForVariables(localStorage.getItem('meterId'))
          this.setState({isOn: localStorage.getItem("meterOn") === "true"})
          this.props.getSchedule(this.props.metersSelected)
        }

        //if (this.state.isHandle && (this.state.isOn !== prevState.isOn || this.state.activeTemp !== prevState.activeTemp || this.state.fan !== prevState.fan || this.state.hot !== prevState.hot || this.state.cold !== prevState.cold)) {
        //  if (this.state.isOn !== prevState.isOn) {
        //    if (this.state.isOn) {
        //      debugger;
        //      this.sendCommand('on');
        //    } else {
        //      debugger;
        //      this.sendCommand('off');
        //    }
        //  } else {
        //    debugger;
        //    this.sendCommand(null); // send command with controllers
        //  }
        //}

        if(this.props.pow && this.props.pow !== prevProps.pow && this.props.pow > 10){
          switch (this.props.lastControl.mode) {
            case "heat":
                this.setState({hot: true, activeTemp: this.props.lastControl.temp })
              break;
            case "fan":
              this.setState({fan: true})
              break;
            case "cool":
              this.setState({cold: true, activeTemp: this.props.lastControl.temp})
              break;
            default:
              break;
          }          
        }

        if(this.props.schedule && this.props.schedule !== prevProps.schedule) {
          const loadSchedule = [];

          loadSchedule[0] = this.props.schedule.mon.prog;
          loadSchedule[0].id = 'Lunes';
          loadSchedule[1] = this.props.schedule.tue.prog;
          loadSchedule[1].id = 'Martes';
          loadSchedule[2] = this.props.schedule.wed.prog;
          loadSchedule[2].id = 'Miercoles';
          loadSchedule[3] = this.props.schedule.thu.prog;
          loadSchedule[3].id = 'Jueves';
          loadSchedule[4] = this.props.schedule.fri.prog;
          loadSchedule[4].id = 'Viernes';
          loadSchedule[5] = this.props.schedule.sat.prog;
          loadSchedule[5].id = 'Sabado';
          loadSchedule[6] = this.props.schedule.sun.prog;
          loadSchedule[6].id = 'Domingo';

          for(let i=0;i<7;i++){
            switch (loadSchedule[i].modo) {
              case 'fan':
                loadSchedule[i].fan='fanOn';
                loadSchedule[i].cold='coldOff';
                loadSchedule[i].hot='hotOff';
              break;
              case 'cold':
                loadSchedule[i].fan='fanOff';
                loadSchedule[i].cold='coldOn';
                loadSchedule[i].hot='hotOff';
              break;
              case 'hot':
                loadSchedule[i].fan='fanOff';
                loadSchedule[i].cold='coldOff';
                loadSchedule[i].hot='hotOn';
              break;
              default:
            }
          }

          this.setState({checkSchedule:loadSchedule});
        }

        if(this.props.showMessage && this.props.showMessage !== prevProps.showMessage) {
            this.commandSended()
        }
    }

    handleStatusClick() {
        let buttonClickTime;
        let buttonInterval;
        if (this.state.active) {
          this.handlePower();
          this.setState({ active: false });
          buttonClickTime = new Date().getTime();
          this.sendCommand('off');
          buttonInterval = setInterval(() => {
            const now = new Date().getTime();
            if (now - buttonClickTime >= 2500) {
              this.setState({
                active: true
              });
              clearInterval(buttonInterval);
            }
          }, 1500);
        } else {
          this.sendCommand('on');
        }
      }

      handlePower() {
        if (this.state.isOn) {
          this.setState({
            isOn: false,
            isHandle: true,
            fan: false,
            hot: false,
            cold: false,
            activeTemp: 22
          });
        } else {
          this.setState({
            isOn: true,
            isHandle: true,
            fan: true,
            hot: false,
            cold: false,
            activeTemp: 22
          });
        }
      }

      sendCommand(command) {
        // alert(command);
        // alert(typeof command);
        const temp = this.state.activeTemp < 18 ? 18 : this.state.activeTemp > 30 ? 30 : this.state.activeTemp;
        let comm = '';
        if (command && typeof command === 'string') {
          comm = command.toUpperCase(); // ON or OFF commands
          if (comm === 'ON') {
            comm = 'ON_AUTO';
          } else if (comm === 'OFF') {
            comm = 'OFF_AUTO';
          }
        } else if (this.state.cold || this.state.hot) { // if HOT or COLD is active
          comm = `ON_${this.state.cold ? 'COOL' : 'HEAT'}_${temp}_AUTO`;
        } else {
          comm = 'ON_FAN_HIGH';
        }
        // sends ON_FAN_HIGH by default
        if (this.props.brand && this.props.brand.toUpperCase() === 'KENDAL') {
          comm = `${this.props.brand.toUpperCase()}_${comm}`;
        }
        if (this.props.brand && this.props.brand.toUpperCase() === 'MOMIT') {
          comm = `${this.props.brand.toUpperCase()}_${comm}`;
        }
        if (this.props.brand && this.props.brand.toUpperCase() === 'DAITSU') {
          comm = `${this.props.brand.toUpperCase()}_${comm}`;
        }
        /*eslint import/namespace: ['error', { allowComputed: true }]*/
        const exec = {
          command: commands[comm] ? commands[comm] : commands.OFF_AUTO,
          device: localStorage.getItem('meterSelected'),
          time: 0,
          start: new Date().getTime()
        };
        this.setState({activeCommand: exec});
        // alert(JSON.stringify(exec));
        clearInterval(commandInterval);
        commandInterval = setInterval(() => {
          const now = new Date().getTime();
          if (now - this.state.activeCommand.start >= 2500) {
            this.props.sendCommandExec(exec);
            if (command && typeof command === 'string' && command.toUpperCase() === 'ON') {
              if (exec.command === commands.ON_AUTO) {
                const fullFan = Object.assign(exec, { command: commands.ON_FAN_HIGH });
                this.props.sendCommandExec(fullFan);
              }
              if (exec.command === commands.KENDAL_ON_AUTO) {
                const fullFan = Object.assign(exec, { command: commands.KENDAL_ON_FAN_HIGH });
                this.props.sendCommandExec(fullFan);
              }
            }
            this.setState({
              sent: true,
              activeCommand: Object.assign(this.state.activeCommand, { finish: now })
            });
            clearInterval(commandInterval);
          }
        }, 500);
      }

    handleGeoReference = () => {
        const location = window.navigator && window.navigator.geolocation
        
        if (location) {
          location.getCurrentPosition((position) => {
            this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
            })
          }, (error) => {
              this.setState({ latitude: '-33.4206423', 
              longitude: '-70.6097705' 
            })
          })
        }
    }

    getColor = () => {
        const blue = '#009fe9';
        const red = '#ff1b49';
        const grey = '#afafaf';
        if(this.state.hot) {
            return red;
        } else if (this.state.cold) {
            return blue;
        } else {
            return grey;
        }
    }

    handleControl(ctrl) {
        if (this.state.isOn) {
          ctrl.fan = !ctrl.hot && !ctrl.cold;
        } else {
          ctrl.isOn = true;
          ctrl.fan = !ctrl.hot && !ctrl.cold;
        }
        ctrl.isHandle = true;
        this.setState(ctrl);
        this.sendCommand('on')
      }

      handleControlSchedule = (i,tipo) => {
        const checkSchedule = this.state.checkSchedule;
        this.updateSchedule._id = this.props.schedule._id;

         switch(i){
           case 0:
            this.updateSchedule.mon.prog.modo = tipo;
           break;
           case 1:
            this.updateSchedule.tue.prog.modo = tipo;
           break;
           case 2:
            this.updateSchedule.wed.prog.modo = tipo;
           break;
           case 3:
            this.updateSchedule.thu.prog.modo = tipo;
           break;
           case 4:
            this.updateSchedule.fri.prog.modo = tipo;
           break;
           case 5:
            this.updateSchedule.sat.prog.modo = tipo;
           break;
           case 6:
            this.updateSchedule.sun.prog.modo = tipo;
           break;
          default:
            break;
         }
         this.props.updateSchedule(this.updateSchedule);
        this.setState({checkSchedule});
      }

    handleChange = (event) => {
        this.setState({age: event.target.value})
    }

    handleChangeTemp = (temp) => {
      if (temp <= this.state.max && temp >= this.state.min) {
        this.handleActiveTemp(temp);
      }
      this.commandCircularSelector()
    }

    handleActiveTemp(temp) {
      this.setState({
        isHandle: true,
        activeTemp: Math.floor(temp)
      });
    }

    handleTimeChangeOff = (event, indice) => {
      const checkSchedule = this.state.checkSchedule;
      this.updateSchedule._id = this.props.schedule._id;
      checkSchedule[indice].turnOff = event.target.value;
        switch(indice){
          case 0:
           this.updateSchedule.mon.prog.turnOff = event.target.value;
          break;
          case 1:
           this.updateSchedule.tue.prog.turnOff = event.target.value;
          break;
          case 2:
           this.updateSchedule.wed.prog.turnOff = event.target.value;
          break;
          case 3:
           this.updateSchedule.thu.prog.turnOff = event.target.value;
          break;
          case 4:
           this.updateSchedule.fri.prog.turnOff = event.target.value;
          break;
          case 5:
           this.updateSchedule.sat.prog.turnOff = event.target.value;
          break;
          case 6:
           this.updateSchedule.sun.prog.turnOff = event.target.value;
          break;
          default:
            break;
        }
        this.props.updateSchedule(this.updateSchedule);
   
       this.setState({checkSchedule});
   }

   handleTimeChangeOn = (event, indice) => {
    const checkSchedule = this.state.checkSchedule;
    this.updateSchedule = this.props.schedule;
    checkSchedule[indice].turnOn = event.target.value;
      switch(indice){
        case 0:
         this.updateSchedule.mon.prog.turnOn = event.target.value;
        break;
        case 1:
         this.updateSchedule.tue.prog.turnOn = event.target.value;
        break;
        case 2:
         this.updateSchedule.wed.prog.turnOn = event.target.value;
        break;
        case 3:
         this.updateSchedule.thu.prog.turnOn = event.target.value;
        break;
        case 4:
         this.updateSchedule.fri.prog.turnOn = event.target.value;
        break;
        case 5:
         this.updateSchedule.sat.prog.turnOn = event.target.value;
        break;
        case 6:
         this.updateSchedule.sun.prog.turnOn = event.target.value;
        break;
        default:
            break;
      }
   
      console.log(this.updateSchedule)
      this.props.updateSchedule(this.updateSchedule);

     this.setState({checkSchedule});
  }

    toggleCheck = (event, indice) => {
      const checkSchedule = this.state.checkSchedule;
      checkSchedule[indice].isActive = !checkSchedule[indice].isActive;
      if (checkSchedule[indice].isActive) {
        //enable
        if (checkSchedule[indice].fan === 'fanOn'){
          this.updateSchedule.modo = 'fan'
        }
        if (checkSchedule[indice].cold === 'fanOn'){
          this.updateSchedule.modo = 'cold'
        }
        if (checkSchedule[indice].hot === 'fanOn'){
          this.updateSchedule.modo = 'hot'
        }
      } else {
        // checkSchedule[indice].fan = fanOff;
       // checkSchedule[indice].cold = coldOff;
       // checkSchedule[indice].hot = hotOff;
      }
      this.updateSchedule._id = this.props.schedule._id;
      this.updateSchedule.mon.prog = checkSchedule[0];
      this.updateSchedule.tue.prog = checkSchedule[1];
      this.updateSchedule.wed.prog = checkSchedule[2];
      this.updateSchedule.thu.prog = checkSchedule[3];
      this.updateSchedule.fri.prog = checkSchedule[4];
      this.updateSchedule.sat.prog = checkSchedule[5];
      this.updateSchedule.sun.prog = checkSchedule[6];
      console.log(this.updateSchedule)
      this.props.updateSchedule(this.updateSchedule);
   
   }

   handleTemp = (i,tipo) => {
    const checkSchedule = this.state.checkSchedule;
    this.updateSchedule._id = this.props.schedule._id;

     switch(tipo){
      case 'sube':
        if (checkSchedule[i].temp < 30 ){
         checkSchedule[i].temp += 1;
        }
       break;
      case 'baja':
       if (checkSchedule[i].temp > 13 ){
         checkSchedule[i].temp -= 1;
       }
         break
       default:
     }
     switch(i){
       case 0:
        this.updateSchedule.mon.prog.temp = checkSchedule[i].temp;
       break;
       case 1:
        this.updateSchedule.tue.prog.temp = checkSchedule[i].temp;
       break;
       case 2:
        this.updateSchedule.wed.prog.temp = checkSchedule[i].temp;
       break;
       case 3:
        this.updateSchedule.thu.prog.temp = checkSchedule[i].temp;
       break;
       case 4:
        this.updateSchedule.fri.prog.temp = checkSchedule[i].temp;
       break;
       case 5:
        this.updateSchedule.sat.prog.temp = checkSchedule[i].temp;
       break;
       case 6:
        this.updateSchedule.sun.prog.temp = checkSchedule[i].temp;
       break;
       default:
         break;
     }
     console.log(this.updateSchedule)
      this.props.updateSchedule(this.updateSchedule);
      this.setState({checkSchedule});
  }
    
    render() {
        return (
            <Container>
                <ToastContainer />
                <div className="home-flex-top">
                    <MeterSelector ></MeterSelector>
                </div>

                <div className="home-flex-middle" style={{ display: `${ this.props.metersSelect && this.props.metersSelect.length === 0 ? "none" : ""  }` }}>
                    <CurrentVariables 
                        handleGeoReference={this.handleGeoReference} actualTemperature={this.props.actualTemperature} 
                        temp={this.props.temp} hum={this.props.hum} pow={this.props.pow}/>

                    
                    <CircularSelector activeTemp={this.state.activeTemp} getColor={this.getColor} fan={this.state.fan} isOn={this.state.isOn} handleChangeTemp={this.handleChangeTemp}/>

                    <div className="actions-to-change">
                        <div className="actions-temperature">
                            <Tooltip className={this.state.isOn ? '' : 'tooltip-disable'} title={`Pulse para ${this.state.isOn ? 'apagar' : 'encender'}`} aria-label="Add" placement="top" onClick={() => this.handleStatusClick()}>
                                <Fab aria-label="Add">
                                    <PowerSettingsNew />
                                </Fab>
                            </Tooltip>
                            <div style={{padding: '18px'}} xs={6}>
                                <span style={{fontFamily: 'Lato, sans-serif',fontSize: 'large',color: this.state.isOn ? '#b1d12a' : '#808080'}}>ON</span>
                                <span style={{fontFamily: 'Lato, sans-serif',fontSize: 'large',color: '#808080'}}>/</span>
                                <span style={{fontFamily: 'Lato, sans-serif',fontSize: 'large',color: !this.state.isOn ? '#606060' : '#808080'}}>OFF</span>
                            </div>
                        </div>
                        
                        <div className="actions-temperature-options">
                            <Tooltip className={this.state.fan && this.state.isOn ? "fan-on" : ''} title={`Ventilador ${this.state.fan && this.state.isOn ? 'Activado' : 'Desactivado'}`} aria-label="Add" placement="top" 
                                onClick={()=> {this.handleControl({fan: !this.props.fan, hot: !this.props.cold && !this.props.fan ? false : this.props.hot, cold: !this.props.fan && !this.props.hot ? false : this.props.cold });}}>
                                <Fab aria-label="Add">
                                    <Toys />
                                </Fab>
                            </Tooltip>
                            <Tooltip className={this.state.hot && this.state.isOn ? "hot-on" : ''} title={`Calor ${this.state.hot && this.state.isOn ? 'Activado' : 'Desactivado'}`} aria-label="Add" placement="top" 
                                onClick={()=> {this.handleControl({hot: !this.props.hot, cold: !this.props.hot && !this.props.fan ? false : this.props.cold, fan: !this.props.cold && !this.props.hot ? false : this.props.fan });}}>
                                <Fab aria-label="Add">
                                    <BrightnessLow />
                                </Fab>
                            </Tooltip>
                            <Tooltip className={this.state.cold && this.state.isOn ? "cold-on" : ''} title={`Frío ${this.state.cold && this.state.isOn ? 'Activado' : 'Desactivado'}`} aria-label="Add" placement="top" 
                                onClick={()=> {this.handleControl({cold: !this.props.cold, hot: !this.props.cold && !this.props.fan ? false : this.props.hot, fan: !this.props.cold && !this.props.hot ? false : this.props.fan });}}>
                                <Fab aria-label="Add">
                                    <AcUnit />
                                </Fab>
                            </Tooltip>
                        </div>
                    </div>
                </div>

                {/* <div style={{float: 'right'}}>
                    <p>Última Medicion: {this.props.meters ? this.props.meters[0].updatedAt : ''}</p>
                </div> */}


                <div className="home-flex-bottom" style={{ marginTop: "1em", display: `${  this.props.metersSelect && this.props.metersSelect.length === 0 ? "none" : ""  }` }}>
                  {this.state.checkSchedule.map((schedule, i) => {
                    return(<DaysProgramation 
                        handleTemp={this.handleTemp}
                              temp={schedule.temp}
                              fan={schedule.fan}
                              hot={schedule.hot}
                              cold={schedule.cold}
                              isActive={schedule.isActive}
                              schedule={schedule || null} 
                              turnOff={schedule.turnOff}
                              turnOn={schedule.turnOn}
                              toggleCheck={this.toggleCheck} 
                              handleTimeChangeOff={this.handleTimeChangeOff}
                              handleTimeChangeOn={this.handleTimeChangeOn}
                              index={i} 
                              key={i} 
                              schedulePosition={i}
                              handleControlSchedule={this.handleControlSchedule}
                            />
                          )
                  })}
                </div>

            </Container>
          );
    }
}

export default withRouter(Home);