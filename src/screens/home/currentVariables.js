import React from 'react';
import Paper from '@material-ui/core/Paper';

const currentVariables = (props) => {

    return(
        <div>
            <Paper className="MuiPaper-root--green" style={{display: 'flex', justifyContent: 'space-around'}}>
                <p className="paragraph--first">Temperatura</p>
                <p className="paragraph paragraph--green">{props.temp} ºC</p>
            </Paper>
            <Paper className="MuiPaper-root--blue" style={{display: 'flex', justifyContent: 'space-around'}}>
                <p className="paragraph--first">Humedad</p>
                <p className="paragraph paragraph--blue">{props.hum} %</p>
            </Paper>
            <Paper className="MuiPaper-root--red" style={{display: 'flex', justifyContent: 'space-around'}}>
                <p className="paragraph--first">Potencia</p>
                <p className="paragraph paragraph--red">{props.pow} W</p>
            </Paper>
            <Paper className="MuiPaper-root--purple" style={{display: 'flex', justifyContent: 'space-around'}}>
                <p className="paragraph--first">Clima local</p>
                <p className="paragraph paragraph--purple">{props.actualTemperature} ºC</p>
            </Paper>
        </div>
    )
}

export default currentVariables