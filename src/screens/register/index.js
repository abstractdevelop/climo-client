import React, { Component } from 'react'
import { FormControl, InputLabel, Input, Container, InputAdornment, IconButton, Button} from '@material-ui/core';
import {EmailOutlined, VisibilityOff, Visibility, Lock, Face, FaceTwoTone, LockTwoTone} from '@material-ui/icons'
import { pink } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/core/styles';
import '../index.scss'


const ColorButton = withStyles(theme => ({
  root: {
    color: theme.palette.getContrastText(pink[400]),
    backgroundColor: pink[400],
    '&:hover': {
      backgroundColor: pink[500],
    },
  },
}))(Button);


export default class Register extends Component {
  constructor(props) {
    super(props)

    this.state = {
      username: '',
      password: '',      
      showPassword: false,
      firstName: '',
      lastName: '',
      passwordConfirm: '',
      showPasswordConfirm: false,
    }
  }

  componentDidUpdate(prevProps, prevState){
    if(this.props.created && this.props.created !== prevProps.created){
      this.props.history.push("/login")
    }
  }

  handleChange = prop => event => {
    this.setState({  [prop]: event.target.value });
  };

  handleClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword, showPasswordConfirm: !this.state.showPasswordConfirm });
  };

  handleSubmit = () => {
    const {username, password, firstName, lastName} = this.state    
    const user = { username: username, password: password, firstName: firstName, lastName: lastName }
    this.props.postUser(user)        
  }

  render() {
    return (              
      <Container component="form" maxWidth="sm" style={{backgroundColor: "white", padding: "1rem"}}>
        <div className="form-container"> 
          <div style={{fontSize: "1.5rem"}}>Registro</div>
                     
            <FormControl>
              <InputLabel htmlFor="input-email">Email</InputLabel>              
              <Input id="input-email" aria-describedby="my-helper-text"
              value={this.state.username}
              onChange={this.handleChange('username')}
              autoComplete="username email"
               startAdornment={
                <InputAdornment position="start">
                   <EmailOutlined></EmailOutlined>
                </InputAdornment>
               }/>            
            </FormControl>  

            <FormControl>
              <InputLabel htmlFor="input-name">Nombre</InputLabel>              
              <Input id="input-name" aria-describedby="my-helper-text"
              value={this.state.firstName}
              onChange={this.handleChange('firstName')}              
               startAdornment={
                <InputAdornment position="start">
                   <Face></Face>
                </InputAdornment>
               }/>            
            </FormControl>  

            <FormControl>
              <InputLabel htmlFor="input-last-name">Apellido</InputLabel>              
              <Input id="input-last-name" aria-describedby="my-helper-text"
              value={this.state.lastName}
              onChange={this.handleChange('lastName')}              
               startAdornment={
                <InputAdornment position="start">
                   <FaceTwoTone></FaceTwoTone>
                </InputAdornment>
               }/>            
            </FormControl>  
          
            <FormControl >
                <InputLabel htmlFor="adornment-password">Contraseña</InputLabel>
                <Input
                  id="adornment-password"
                  type={this.state.showPassword ? 'text' : 'password'}
                  value={this.state.password}
                  onChange={this.handleChange('password')}
                  autoComplete="new-password"
                  startAdornment={
                    <InputAdornment position="start">
                      <Lock></Lock>
                    </InputAdornment>
                  }
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton aria-label="Toggle password visibility" onClick={this.handleClickShowPassword}>
                        {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>      

              <FormControl >
                <InputLabel htmlFor="adornment-password-confirm">Repetir Contraseña</InputLabel>
                <Input
                  id="adornment-password-confirm  "
                  type={this.state.showPasswordConfirm ? 'text' : 'password'}
                  value={this.state.passwordConfirm}
                  onChange={this.handleChange('passwordConfirm')}
                  autoComplete="new-password"
                  startAdornment={
                    <InputAdornment position="start">
                      <LockTwoTone></LockTwoTone>
                    </InputAdornment>
                  }
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton aria-label="Toggle password visibility" onClick={this.handleClickShowPassword}>
                        {this.state.showPasswordConfirm ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>  

              <div style={{margin: "1rem 0", display: "flex", flexDirection: "row-reverse"}}>
                <ColorButton onClick={this.handleSubmit} variant="contained">Registrarme</ColorButton>  
              </div>
              
              

        </div>                    
      </Container>
    )
  }
}
