import React, { Component } from "react";
import { Grid, Container, Button } from "@material-ui/core";
import { Info, Devices } from "@material-ui/icons";
import InfoUser from './infoUser';
import ListMeters from './listMeters';
import "./index.scss";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      profile: {},
      currentSection: "dataUser",
      meters: [],
    };
  }

  componentDidMount() {
    this.props.getProfile();
    if(this.props.meters === null){
      this.props.getMeters(localStorage.getItem("id"))
    }else{
      this.setState({meters: this.props.meters})
    }
  }
  
  componentDidUpdate(prevProps, prevState)  {
    
    if(this.props.profile && this.props.profile !== prevProps.profile){
      this.setState({
        email: this.props.profile.email,
        profile: this.props.profile.profile
      })      
    }
    if(this.props.updated !== prevProps.updated){
      if(this.props.updated){
        toast.success("Se han actualizado los datos")
      }
    }

    if(this.props.meters && this.props.meters !== prevProps.meters){
      this.setState({meters: this.props.meters})
    }
    if(this.props.meter && this.props.meter !== prevProps.meter){
      this.props.getMeters(localStorage.getItem("id"))
    }

  }

  setSelection = dataShow => {
    if(dataShow) {
      this.setState({
        currentSection: dataShow
      })
    }
  }

  checkSection = section => {
    if (section !== this.state.currentSection){
      return ""
    }
    switch (this.state.currentSection) {
      case "dataUser":
        return "active--my-info"        
      case "dataMeters":
        return "active--my-meters"
      default:
        return ""
    }
  }

  render() {

    const { ...profile } = this.state.profile    
    
    return (
      <div style={{display: "flex", flexDirection: "column"}}>   
        <Container className="profile-wrapper" maxWidth="lg">
          <ToastContainer></ToastContainer>
          <Grid container spacing={6}>
            <Grid container item xs={4} direction="column" justify="flex-start" className="sidebar-profile" >
              <div className="profile-button__container">
                <Button className={`${this.checkSection("dataUser")}`} onClick={()=>this.setSelection("dataUser")}><span>Mis Datos</span> <Info className="icon-profile" /></Button> 
                <Button className={`${this.checkSection("dataMeters")}`} onClick={()=>this.setSelection("dataMeters")}><span>Mi Equipo</span> <Devices className="icon-profile" /></Button> 
                {/* <Button className={`class-profile`}><span>Usuarios</span> <PersonAdd className="icon-profile" /></Button>  */}
              </div>
          </Grid>
          <Grid item xs={8} style={{background:"#fff", marginTop:"2.5em"}}>
              <Grid container spacing={3}>
                { 
                  this.state.currentSection === "dataUser" ? <InfoUser email={this.state.email} profile={profile} infoUser={this.props.profile} putUser={this.props.putUser} getProfile={this.props.getProfile} /> :
                  this.state.currentSection === "dataMeters" ? <ListMeters meters={this.state.meters} putMeterLabel={this.props.putMeterLabel} /> : "sin selección"
                }

              </Grid>
            </Grid>            
          </Grid>
        </Container>
      </div>
    );
  }
}

export default User;