import React, {Fragment} from 'react';
import { Grid, List, ListItem, ListItemAvatar, ListItemText, Avatar } from "@material-ui/core";
import { Info, AccountCircle, PhoneIphone, Email, LocationOn, Wc, Cake, School } from "@material-ui/icons";
import ModalForm from './modal';

const InfoUser = ({ email, profile, infoUser, putUser, getProfile }) => {
    return (
        <Fragment>

            <Grid container style={{ borderBottom: "1px solid #eaeaea" }}>
                <Grid item xs={4}>
                    <h1>Tus Datos</h1>
                    <ModalForm infoUser={infoUser} putUser={putUser} getProfile={getProfile}></ModalForm>
                </Grid>
            </Grid>

            <Grid item xs={6}>
            <List>
                
                <ListItem>
                    <ListItemAvatar>
                        <Avatar>
                            <AccountCircle></AccountCircle>
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary="Nombre:" secondary={`${profile.firstName} ${profile.lastName}`} />
                </ListItem>

                <ListItem>
                    <ListItemAvatar>
                        <Avatar>
                            <Info></Info>
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary="Rut:" secondary={profile.rut} />
                </ListItem>

                <ListItem>
                    <ListItemAvatar>
                        <Avatar>
                            <Email></Email>
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary="Email:" secondary={email} />
                </ListItem>

                <ListItem>
                    <ListItemAvatar>
                        <Avatar>
                            <School></School>
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary="Profesión:" secondary={profile.occupation} />
                </ListItem>

            </List>
      </Grid>

            <Grid item xs={6}>
            <List>

                <ListItem>
                    <ListItemAvatar>
                        <Avatar>
                            <PhoneIphone></PhoneIphone>
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary="Teléfono:" secondary={profile.phone} />
                </ListItem>

                <ListItem>
                    <ListItemAvatar>
                        <Avatar>
                            <LocationOn></LocationOn>
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary="Dirección:" secondary={profile.address} />
                </ListItem>

                <ListItem>
                    <ListItemAvatar>
                        <Avatar>
                            <Wc></Wc>
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary="Género:" secondary={profile.gender}/>
                </ListItem>

                <ListItem>
                    <ListItemAvatar>
                        <Avatar>
                            <Cake></Cake>
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary="Fecha de nacimiento:" secondary={profile.birthdate ?  profile.birthdate.split("T")[0] : ""}/>
                </ListItem>
            
            </List>     
        </Grid>

        </Fragment>
    );
}
 
export default InfoUser;