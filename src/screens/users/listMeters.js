import React, { Fragment } from 'react';
import { Grid } from "@material-ui/core";
import EditMeter from './editMeter'

function ListMeters({ meters, putMeterLabel }){

    const handleForm = (meterId, label) => {

        const meter = { _id: meterId, label: label}
        if(meter){   
            putMeterLabel(meter)
        }
    }

    if(meters){
        return(
            <Fragment>
                <Grid container style={{ borderBottom: "1px solid #eaeaea" }}>
                    <Grid item xs={4}>
                        <h1> Tus Equipos</h1>
                    </Grid>
                </Grid>
                {/* list meters divs  */}
                <div className="all-meters-user">
                    <div className="meter-header">
                        <div className="cell-info-meter">Id Dispositivo</div>
                        <div className="cell-info-meter">Nombre</div>
                        <div className="cell-info-meter">Marca</div>
                        <div className="cell-info-meter">Modelo</div>
                        <div className="cell-info-meter">Acciones</div>
                    </div>
                    {meters.map(meter => (
                        <div className="meter-item" key={meter._id}>
                            <div className="cell-info-meter">{meter.deviceId}</div>
                            <div className="cell-info-meter">{meter.label}</div>
                            <div className="cell-info-meter">{meter.brand}</div>
                            <div className="cell-info-meter">{meter.model}</div>
                            <div className="cell-info-meter table-update-label-btn"><EditMeter meterLabel={meter.label} meterId={meter._id} handleForm={handleForm} /></div>
                        </div>
                    ))}
                </div>

            </Fragment>
    )
    } else {
        return (
            <p>¡No existe ningun equipo asociado para esta cuenta!</p>
        )
    }
}

export default ListMeters; 