import React, { useState, Fragment } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Modal, TextField, Button, Select, MenuItem, InputLabel, Input, FormControl } from "@material-ui/core";


function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  };
}

const useStyles = makeStyles(theme => ({
  paper: {
    position: "absolute",
    maxWidth: 700,
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #8a8a8a",
    boxShadow: theme.shadows[5],
    borderRadius: "5px",
    padding: theme.spacing(2, 4, 4),
    outline: "none",
    overflow: "scroll",
    width: "100%"
  }
}));

export default function SimpleModal({ infoUser, putUser, getProfile }) {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  function UserForm({ infoUser, putUser, getProfile, handleClose }) {
  
    const [profileForm, handleProfileForm] = useState({
      _id: infoUser._id,
      firstName: infoUser.profile.firstName,
      lastName: infoUser.profile.lastName,
      rut: infoUser.profile.rut,
      email: infoUser.email,
      phone: infoUser.profile.phone,
      address: infoUser.profile.address,
      occupation: infoUser.profile.occupation,
      birthdate: infoUser.profile.birthdate,
      gender: infoUser.profile.gender,
      password: '',
      repeatPass: '',
    });
    
    const handleChange = e => {
      handleProfileForm({
        ...profileForm,
        [e.target.name]: e.target.value
      });
    };

    const handleSubmit = () => {
      if(profileForm.password === profileForm.repeatPass){
        delete profileForm.repeatPass
        putUser(profileForm)
        getProfile()
        handleClose()
      } else {
        alert("Los campos de contraseña no coinciden.")
      }
    };

    return (
      <Fragment>
        <form noValidate autoComplete="off" className="form-profile">
          <Grid container spacing={3}>
            <Grid item xs={4}>
              <TextField
                id="firstName"
                label="Nombres"
                placeholder="Ejemplo. Luis Mercado Ariza"
                margin="normal"
                name="firstName"
                value={profileForm.firstName}
                onChange={handleChange}
                fullWidth
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                id="lastName"
                label="Apellidos"
                placeholder="Ejemplo. Luis Mercado Ariza"
                margin="normal"
                name="lastName"
                value={profileForm.lastName}
                onChange={handleChange}
                fullWidth
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                id="rut"
                label="Rut"
                placeholder="Ejemplo. 11.111.111-1"
                margin="normal"
                name="rut"
                value={profileForm.rut}
                onChange={handleChange}
                fullWidth
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                autoComplete="username email"
                id="email"
                label="Email"
                placeholder="Ejemplo. luis@email.com"
                margin="normal"
                name="email"
                disabled
                value={profileForm.email}
                onChange={handleChange}
                fullWidth
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                id="phone"
                label="Teléfono"
                placeholder="Ejemplo. 569 9358 9664"
                margin="normal"
                name="phone"
                value={profileForm.phone}
                onChange={handleChange}
                fullWidth
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="address"
                label="Dirección"
                placeholder="Ejemplo. Calle Providencia #1010. Depto. 101. Providencia"
                margin="normal"
                name="address"
                value={profileForm.address}
                onChange={handleChange}
                fullWidth
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                id="standard-with-placeholder"
                label="Profesión"
                placeholder="Ejemplo. Abogado"
                margin="normal"
                name="occupation"
                value={profileForm.occupation}
                onChange={handleChange}
                fullWidth
              />
            </Grid>
            <Grid item xs={4}>
              <TextField
                id="date"
                label="Fecha de Nacimiento"
                type="date"
                margin="normal"
                name="birthdate"
                value={profileForm.birthdate ?  profileForm.birthdate.split("T")[0] : ""}
                onChange={handleChange}
                fullWidth
                InputLabelProps={{
                  shrink: true
                }}
              />
            </Grid>
            <Grid item xs={4}>         
              <FormControl fullWidth margin="normal"  >
                <InputLabel shrink htmlFor="gender" >
                  Género
                </InputLabel>
                <Select onChange={handleChange}  value={profileForm.gender} fullWidth  displayEmpty    
                input={<Input name="gender" id="gender" />}          
                  >
                  <MenuItem value="" disabled>
                    Ejemplo. Mujer
                  </MenuItem>
                  <MenuItem value='Hombre'>Hombre</MenuItem>
                  <MenuItem value='Mujer'>Mujer</MenuItem>
                  <MenuItem value='No Especifica'>No Especifica</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <strong>Cambiar Contraseña</strong>
              <hr></hr>  
            </Grid>
            <Grid item xs={6}>
              
              <TextField
                id="password"
                label="Nueva contraseña"
                autoComplete="new-password"
                margin="normal"
                type="password"
                name="password"
                value={profileForm.password}
                onChange={handleChange}
                fullWidth
              />
            </Grid>

            <Grid item xs={6}>
              <TextField
                id="repeatPass"
                label="Repetir contraseña"
                autoComplete="new-password"
                margin="normal"
                name="repeatPass"
                type="password"
                onChange={handleChange}
                fullWidth
              />
            </Grid>
            

            <Grid item xs={12} style={{ textAlign: "right" }}>
              <Button
                onClick={handleClose}
                variant="contained"
                size="large"
                className="btn-cancel-profile"
              >
                Cancelar
              </Button>

              <Button
                onClick={handleSubmit}
                variant="contained"
                size="large"
                className="btn-update-profile"
              >
                Actualizar
              </Button>
            </Grid>
          </Grid>
        </form>
      </Fragment>
    );
  }

  return (
    <Fragment>
      <Button variant="contained" onClick={handleOpen} size="medium" className="btn-edit-profile">
        Editar
      </Button>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={open}
        onClose={handleClose}
        className="modal-form-user"
      >
        <div style={modalStyle} className={classes.paper}>
          <h2 id="modal-title" className="profile-title-form">Actualizar Datos</h2>
          <UserForm infoUser={infoUser} putUser={putUser} getProfile={getProfile} handleClose={handleClose}/>
        </div>
      </Modal>
    </Fragment>
  );
}
