
import React, {useState, useEffect}from 'react';
import { Button, TextField, Dialog, DialogActions, DialogContent, DialogTitle }  from '@material-ui/core';

function EditMeter({ meterId, meterLabel, handleForm }) {
  const [open, setOpen] = useState(false);
  const [label, setLabel] = useState('');
  const [_id, setId] = useState('');

  useEffect(()=> {
    if(label==='' || _id === ''){
        setLabel(meterLabel)
        setId(meterId)
    }
  },[label, _id])

  const handleChange = e => {    
    setLabel(e.target.value);
  };

  function sendNewLabel(label) {
    console.log(_id, label)
    if(label !== '') {
      handleClose()
      handleForm(_id, label)
    }
  };

  function handleClickOpen() {
    setOpen(true);
  };

  function handleClose() {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Renombrar
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Renombrar dispositivo</DialogTitle>
        <DialogContent style={{width: '400px'}}>
          <TextField
            autoFocus
            margin="dense"
            name="label"
            id="label"
            label="Nombre del Dispositivo"
            value={label}
            onChange={handleChange}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancelar
          </Button>
          <Button onClick={() => sendNewLabel(label)} color="primary">
            Actualizar 
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default EditMeter;