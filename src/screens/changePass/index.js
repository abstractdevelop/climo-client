import React, { Component } from 'react'
import { FormControl, InputLabel, Input, Container, InputAdornment, IconButton, Button} from '@material-ui/core';
import { VisibilityOff, Visibility, Lock, LockTwoTone} from '@material-ui/icons'
import { pink } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/core/styles';
import '../index.scss'


const ColorButton = withStyles(theme => ({
  root: {
    color: theme.palette.getContrastText(pink[400]),
    backgroundColor: pink[400],
    '&:hover': {
      backgroundColor: pink[500],
    },
  },
}))(Button);


export default class Register extends Component {
  constructor(props) {
    super(props)

    this.state = {
      token: '',
      password: '',      
      showPassword: false,
      passwordConfirm: '',
      showPasswordConfirm: false,
      checkPass: true,
    }
  }

  componentDidMount(){
    this.setState({token: this.props.location.pathname.split("/")[2]})
  }

  componentDidUpdate(prevProps, prevState){
    if(this.state.passwordConfirm.length > 0 && this.state.passwordConfirm !== prevState.passwordConfirm ){
      this.setState({ checkPass: this.state.passwordConfirm !== this.state.password})      
    }

    if(this.props.changed && this.props.changed !== prevProps.changed){
      this.props.history.push("/login")
    }
  }

  handleChange = prop => event => {
    this.setState({  [prop]: event.target.value });
  };

  handleClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword, showPasswordConfirm: !this.state.showPasswordConfirm });
  };

  handleSubmit = () => {
    const {password, token} = this.state    
    this.props.postChangePassword(password, token)        
  }

  render() {
    return (              
      <Container component="form" maxWidth="sm" style={{backgroundColor: "white", padding: "1rem"}}>
        <div className="form-container"> 
          <div style={{fontSize: "1.5rem"}}>Cambiar Contraseña</div>
                     
            <FormControl >
                <InputLabel htmlFor="adornment-password">Contraseña</InputLabel>
                <Input
                  id="adornment-password"
                  type={this.state.showPassword ? 'text' : 'password'}
                  value={this.state.password}
                  onChange={this.handleChange('password')}
                  autoComplete="new-password"
                  startAdornment={
                    <InputAdornment position="start">
                      <Lock></Lock>
                    </InputAdornment>
                  }
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton aria-label="Toggle password visibility" onClick={this.handleClickShowPassword}>
                        {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>      

              <FormControl >
                <InputLabel htmlFor="adornment-password-confirm">Repetir Contraseña</InputLabel>
                <Input
                  id="adornment-password-confirm  "
                  type={this.state.showPasswordConfirm ? 'text' : 'password'}
                  value={this.state.passwordConfirm}
                  onChange={this.handleChange('passwordConfirm')}
                  autoComplete="new-password"
                  startAdornment={
                    <InputAdornment position="start">
                      <LockTwoTone></LockTwoTone>
                    </InputAdornment>
                  }
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton aria-label="Toggle password visibility" onClick={this.handleClickShowPassword}>
                        {this.state.showPasswordConfirm ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>  

              <div style={{margin: "1rem 0", display: "flex", flexDirection: "row-reverse"}}>
                <ColorButton onClick={this.handleSubmit} disabled={this.state.checkPass} variant="contained">Cambiar contraseña</ColorButton>  
              </div>
              
              

        </div>                    
      </Container>
    )
  }
}
