import React, { Component } from 'react'
import { FormControl, InputLabel, Input, Container, InputAdornment, Button} from '@material-ui/core';
import {EmailOutlined} from '@material-ui/icons'
import { pink } from '@material-ui/core/colors';
import { withStyles } from '@material-ui/core/styles';
import '../index.scss'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const ColorButton = withStyles(theme => ({
  root: {
    color: theme.palette.getContrastText(pink[400]),
    backgroundColor: pink[400],
    '&:hover': {
      backgroundColor: pink[500],
    },
  },
}))(Button);


export default class Register extends Component {
  constructor(props) {
    super(props)

    this.state = {
      email: '',
    }
  }


  handleChange = prop => event => {
    this.setState({  [prop]: event.target.value });
  };


  handleSubmit = () => {
    const {email} = this.state        
    this.props.recoverPassword(email)        
  }

  componentDidUpdate(prevProps, prevState){
    if(this.props.data && this.props.data !== prevProps.data  && this.props.data.success){
      toast.success("Se ha enviado un email con las instrucciones");
    }
  }

  render() {
    return (              
      <Container component="form" maxWidth="sm" style={{backgroundColor: "white", padding: "1rem"}}>
        <ToastContainer />
        <div className="form-container"> 
          <div style={{fontSize: "1.5rem"}}>Recuperar contraseña</div>
                     
            <FormControl>
              <InputLabel htmlFor="input-email">Ingrese Email</InputLabel>              
              <Input id="input-email" aria-describedby="my-helper-text"
              value={this.state.username}
              onChange={this.handleChange('email')}
              autoComplete="username email"
               startAdornment={
                <InputAdornment position="start">
                   <EmailOutlined></EmailOutlined>
                </InputAdornment>
               }/>            
            </FormControl>  
            

              <div style={{margin: "1rem 0", display: "flex", flexDirection: "row-reverse"}}>
                <ColorButton onClick={this.handleSubmit} variant="contained">Recuperar</ColorButton>  
              </div>
              
              

        </div>                    
      </Container>
    )
  }
}
