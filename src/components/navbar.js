import React, { Component } from 'react';
import { Container, Tabs, Tab} from '@material-ui/core';
import { withRouter } from "react-router-dom";
import './index.scss';
import { removeAuth, loadAuth } from "../helpers/auth";


class Navbar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: 0,      
    }    
  }

  checkAuth(){
    const auth = loadAuth()
    const route =  this.props.location.pathname.split("/")[1]
    if(auth === null && !["login", "recover", "register", "change"].includes(route) ){      
       this.props.history.push("/login");
    }
  }

  checkPath(){    
    let bar = document.getElementsByClassName("PrivateTabIndicator-root-30")[0] || document.getElementsByClassName("jss31")[0]
    if(this.props.location.pathname === "/" && bar){
      this.setState({value: 0})
      bar.style.backgroundColor= "#ff4225";
    }else if(this.props.location.pathname === "/stats" && bar){
      this.setState({value: 1})
      bar.style.backgroundColor= "#009fe9";
    }else if(this.props.location.pathname === "/users" && bar){
      this.setState({value: 2})
      bar.style.backgroundColor= "#b1d12a";
    }else if(this.props.location.pathname === "/login" && bar){        
      this.setState({value: 4})
      bar.style.backgroundColor= "#009fe9";
    }else if(this.props.location.pathname === "/register" && bar){
      this.setState({value: 5})
      bar.style.backgroundColor= "#b1d12a";
    }
  }

  checkTab(valueTab){
    const {value} = this.state
    if( value === valueTab){
      switch (value) {
        case 0:        
          return("tab--home")
        case 1:
            return("tab--stats")
        case 2:
            return("tab--users")
        case 3:
            return("tab--logout")
        case 4:
            return("tab--login")
        case 5:
            return("tab--register")
        default:
            return ""
      }    
    }else{
      return ""
    }
  }

  componentDidMount(){
    this.checkPath()
    this.checkAuth()
  }

  componentDidUpdate(prevProps, prevState){
    if(this.props.location.pathname !== prevProps.location.pathname){
      this.checkPath()
    }
    if(this.state.value !== prevState.value){
      this.checkPath()
    }
  }
  

  handleChangeTab(event, value) {    
    this.setState({value: value});
    switch (value) {
      case 0:        
        this.props.history.push("/")
        break;
      case 1:
        this.props.history.push("/stats")
        break;
      case 2:
        this.props.history.push("/users")
        break;
      case 3:        
        removeAuth();
        this.props.history.push("/login");
        break;
      case 4:
        this.props.history.push("/login")
        break;
      case 5:
        this.props.history.push("/register")
        break;
      default:
        break;
    }
  }


  render() {
    const auth = loadAuth()
    return (
      <div style={{width: "100%", backgroundColor: "white", marginBottom: "1rem"}}>
        <div>
          <Container maxWidth="md" style={{display: "flex", justifyContent: "space-between"}}>   
            <img src={require('../assets/logoclimo.svg')} alt="logo" width="130"></img>                 
            <Tabs
                value={this.state.value}
                onChange={this.handleChangeTab.bind(this)}
                indicatorColor="primary"
                textColor="primary"
                variant="fullWidth"
                className="custom-navbar"
              >
                <Tab style={{display: `${auth ? "inline-flex" : "none" }`}} className={this.checkTab(0)}  label="Mi Climo" />
                <Tab style={{display: `${auth ? "inline-flex" : "none" }`}} className={this.checkTab(1)}  label="Monitoreo" />
                <Tab style={{display: `${auth ? "inline-flex" : "none" }`}} className={this.checkTab(2)}  label="Perfil" />
                <Tab style={{display: `${auth ? "inline-flex" : "none" }`}} className={this.checkTab(3)}  label="Salir" />
                <Tab style={{display: `${auth ? "none" : "inline-flex" }`}} className={this.checkTab(4)}  label="Login" />
                <Tab style={{display: `${auth ? "none" : "inline-flex" }`}} className={this.checkTab(5)} label="Registro" />
              </Tabs>
          </Container>
        </div>
      </div>
    )
  }
}

export default withRouter(Navbar);
