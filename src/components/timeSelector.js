import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';


const menuItems = () => {

    return(
        <div>
            <MenuItem value='0'>12:00 AM</MenuItem>
            <MenuItem value='1800'>12:30 AM</MenuItem>
            <MenuItem value='3600'>01:00 AM</MenuItem>
            <MenuItem value='5400'>01:30 AM</MenuItem>
            <MenuItem value='7200'>02:00 AM</MenuItem>
            <MenuItem value='9000'>02:30 AM</MenuItem>
            <MenuItem value='10800'>03:00 AM</MenuItem>
            <MenuItem value='12600'>03:30 AM</MenuItem>
            <MenuItem value='14400'>04:00 AM</MenuItem>
            <MenuItem value='16200'>04:30 AM</MenuItem>
            <MenuItem value='18000'>05:00 AM</MenuItem>
            <MenuItem value='19800'>05:30 AM</MenuItem>
            <MenuItem value='21600'>06:00 AM</MenuItem>
            <MenuItem value='23400'>06:30 AM</MenuItem>
            <MenuItem value='25200'>07:00 AM</MenuItem>
            <MenuItem value='27000'>07:30 AM</MenuItem>
            <MenuItem value='28800'>08:00 AM</MenuItem>
            <MenuItem value='30600'>08:30 AM</MenuItem>
            <MenuItem value='32400'>09:00 AM</MenuItem>
            <MenuItem value='34200'>09:30 AM</MenuItem>
            <MenuItem value='36000'>10:00 AM</MenuItem>
            <MenuItem value='37800'>10:30 AM</MenuItem>
            <MenuItem value='39600'>11:00 AM</MenuItem>
            <MenuItem value='41400'>11:30 AM</MenuItem>
            <MenuItem value='43200'>12:00 PM</MenuItem>
            <MenuItem value='45000'>12:30 PM</MenuItem>
            <MenuItem value='46800'>01:00 PM</MenuItem>
            <MenuItem value='48600'>01:30 PM</MenuItem>
            <MenuItem value='50400'>02:00 PM</MenuItem>
            <MenuItem value='52200'>02:30 PM</MenuItem>
            <MenuItem value='54000'>03:00 PM</MenuItem>
            <MenuItem value='55800'>03:30 PM</MenuItem>
            <MenuItem value='57600'>04:00 PM</MenuItem>
            <MenuItem value='59400'>04:30 PM</MenuItem>
            <MenuItem value='61200'>05:00 PM</MenuItem>
            <MenuItem value='63000'>05:30 PM</MenuItem>
            <MenuItem value='64800'>06:00 PM</MenuItem>
            <MenuItem value='66600'>06:30 PM</MenuItem>
            <MenuItem value='68400'>07:00 PM</MenuItem>
            <MenuItem value='70200'>07:30 PM</MenuItem>
            <MenuItem value='72000'>08:00 PM</MenuItem>
            <MenuItem value='73800'>08:30 PM</MenuItem>
            <MenuItem value='75600'>09:00 PM</MenuItem>
            <MenuItem value='77400'>09:30 PM</MenuItem>
            <MenuItem value='79200'>10:00 PM</MenuItem>
            <MenuItem value='81000'>10:30 PM</MenuItem>
            <MenuItem value='82800'>11:00 PM</MenuItem>
            <MenuItem value='84600'>11:30 PM</MenuItem>
        </div>
    )
}

export default menuItems;