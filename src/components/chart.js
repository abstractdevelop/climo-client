import React, { Component } from 'react'
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts/highstock';



export default class Chart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // To avoid unnecessary update keep all options in the state.
      chartOptions: {    
        credits: {
          enabled: false
        },    
        title: {
          text: ''
        },
        yAxis: {
          labels: {
              format: '{value}'
          }
        },
        tooltip: {
          valueDecimals: 2,        
          valueSuffix: ''
        },
        rangeSelector: {
          enabled: true,
          selected: 5,
          buttons: [{
            type: 'day',
            count: 1,
            text: '1d'
          }, {
            type: 'day',
            count: 7,
            text: '7d'
          }, {
            type: 'month',
            count: 1,
            text: '1m'
          },{
            type: 'month',
            count: 6,
            text: '6m'
          }, {
            type: 'year',
            count: 1,
            text: '1a'
          }, {
            type: 'all',
            text: 'Todos'
          }]
        },
        series: []
      },
    };
  }

  componentDidUpdate(prevProps, prevState){

    if(this.props.series && this.props.series !== prevProps.series){
      this.setState({
        chartOptions: {
          series: this.props.series
        }
      });      
    }
    if(this.state.chartOptions.series !== prevState.chartOptions.series){      
      this.setState({
        chartOptions: {
          rangeSelector:  {
            selected: 1
          },
          title: {
            text: this.props.title
          },
          yAxis: {
            labels: {
                format: this.props.yAxisLabelFormat,
            }
          },
          tooltip: this.props.tooltip,
        }
      });
    }
  }

  componentDidMount(){
    Highcharts.setOptions({
      lang: {
        //Meses
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        
        shortMonths: ['Ene', 'Febr', 'Marz', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sept', 'Oct', 'Nov', 'Dec'],
        //Días
        weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],

        rangeSelectorFrom: "Desde",
        rangeSelectorTo: "Hasta"
      },  
    });
  }

  render() {
    return (
      <div style={{width: "100%", backgroundColor: "white", padding: "2rem"}}>        
        <HighchartsReact
          highcharts={Highcharts}
          constructorType={'stockChart'}
          options={this.state.chartOptions}
        />
      </div>
    )
  }
}
