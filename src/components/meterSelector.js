import React, { Component } from 'react'
import { FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import {PowerSettingsNew} from '@material-ui/icons';
import CircularProgress from '@material-ui/core/CircularProgress';


export default class MeterSelector extends Component {
  constructor(props) {
    super(props);

    this.state = {
      meterSelected: 0,
      meterOn: false,
      meterId: '',

      displayMessage: false,
    }
      
    this.timer = setTimeout(this.enableMessage.bind(this), 2000);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  enableMessage() {
    this.setState({displayMessage: true});
  }


  handleChange(event) {    
    this.setState({
      [event.target.name]: event.target.value,
      meterOn: event.currentTarget.getAttribute("data-on") === "true",
      meterId: event.currentTarget.getAttribute("data-id")
    });
  }

  componentDidUpdate(prevProps, prevState){
    if(this.props.metersSelect && this.props.metersSelect !== prevProps.metersSelect && this.props.metersSelect.length !== 0 && this.state.meterSelected === 0){      
      this.setState({meterSelected: this.props.metersSelect[0].value, meterOn: this.props.metersSelect[0].on, meterId: this.props.metersSelect[0].id})      
    }
    if(this.state.meterSelected && this.state.meterSelected !== prevState.meterSelected){      
      localStorage.setItem("meterSelected", this.state.meterSelected)
      localStorage.setItem("meterOn", this.state.meterOn)
      localStorage.setItem("meterId", this.state.meterId)
      this.props.setMeterSelected(this.state.meterSelected)      
    }

  }

  componentDidMount(){    
    const meterSelected = localStorage.getItem("meterSelected")
    const meterOn = localStorage.getItem("meterOn") === "true"
    const meterId = localStorage.getItem("meterId")
    if(meterSelected){      
      this.setState({meterSelected, meterOn, meterId})
    }
    if(!this.props.metersSelect){      
      this.props.getMeters(localStorage.getItem("id"))
    }
  }

  render() {
    const {meterSelected} = this.state
    if (this.props.metersSelect && this.props.metersSelect.length !== 0){
      return (
        <div className="meter-selector" >
          <PowerSettingsNew fontSize="large" style={{margin: "0.5rem", color: `${this.state.meterOn ? "#7EBF31" : "gray"  }`, vectorEffect: "non-scaling-stroke", marginLeft: "0" }}></PowerSettingsNew>
          <FormControl>            
            <InputLabel htmlFor="meterSelected-simple">Dispositivo</InputLabel>
            <Select 
              value={meterSelected}
              onChange={this.handleChange.bind(this)}
              inputProps={{
                name: 'meterSelected',
                id: 'meterSelected-simple',
              }}
            >
              {this.props.metersSelect.map( meter => <MenuItem key={meter.key} value={meter.value}  data-on={meter.on} data-id={meter.id}> {meter.name} </MenuItem> )}
            </Select>
        </FormControl>
        </div>
      )
    }else{
      if(this.state.displayMessage){
        return <h1>Usted no posee Dispositivos</h1>
      }else{
        return  <CircularProgress></CircularProgress>
      }
    }
  }
}
