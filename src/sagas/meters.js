import constants from '../constants'
import * as apiRoutes from '../constants/apiRoutes';
import { put, takeEvery} from 'redux-saga/effects'
import { getMetersFetched } from '../actions/meters'
import * as apiCall from '../api/req';

 

/* Meters */
export function * getMetersSaga() {
  yield takeEvery(constants.GET_METERS, callGetMeters)
}

function * callGetMeters(action) {
  const {userId} = action.payload  
  try {
    const result = yield (apiCall.doRequest({
      method: 'GET',
      url: apiRoutes.METERS_URL.replace(apiRoutes.USER_ID, userId),      
    }))    
    yield put(getMetersFetched(result.data))
  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
      // yield put(logInError(e))
      //yield callApiError(action, e)
  }
}