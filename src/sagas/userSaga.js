import constants from '../constants'
import * as apiRoutes from '../constants/apiRoutes';
import { put, takeEvery} from 'redux-saga/effects'
import { postUserFetched, postChangePasswordFetched } from '../actions/user'
import * as apiCall from '../api/req';

 

/* Create User */
export function * postUserSaga() {
  yield takeEvery(constants.POST_USER, callPostUser)
}

function * callPostUser(action) {
  const {user} = action.payload  
  try {
    const result = yield (apiCall.doRequest({
      method: 'POST',
      url: apiRoutes.USER_URL,
      data: user
    }))    
    yield put(postUserFetched(result.data))

  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
      // yield put(logInError(e))
      //yield callApiError(action, e)
  }
}

/* Change pass User */
export function * postChangePassSaga() {
  yield takeEvery(constants.CHANGE_PASSWORD, callPostChangePass)
}

function * callPostChangePass(action) {
  const {password, token} = action.payload  
  try {
    const result = yield (apiCall.doRequest({
      method: 'POST',
      url: apiRoutes.CHANGE_PASS_URL,
      data: {password: password, token: token}
    }))    
    yield put(postChangePasswordFetched(result.data))

  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
      // yield put(logInError(e))
      //yield callApiError(action, e)
  }
}