import constants from '../constants'
import * as apiRoutes from '../constants/apiRoutes';
import { put, takeEvery} from 'redux-saga/effects'
import { getScheduleSuccess } from '../actions/scheduleActions'
import * as apiCall from '../api/req';

 

/* Stats */
export function * getScheduleSaga() {
  yield takeEvery(constants.GET_SCHEDULE, getSchedule)
}

function * getSchedule(action) {
  const deviceId = action.payload.deviceId
  try {
    const result = yield (apiCall.doRequest({
      method: 'GET',
      url: apiRoutes.SEARCH_SCHEDULE_BY_METER_ID_URL.replace(apiRoutes.METER_ID, deviceId),      
    }))
    yield put(getScheduleSuccess(result.data))

  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
  }
}

/* update */

export function * updateScheduleSaga() {
  yield takeEvery(constants.UPDATE_SCHEDULE, updateSchedule)
}

function * updateSchedule(action) {
  const schedule = action.payload.schedule
  try {
    const result = yield (apiCall.doRequest({
      method: 'PUT',
      url: apiRoutes.UPDATE_SCHEDULE_URL.replace(apiRoutes.SCHEDULE_ID, schedule._id),
      data: schedule
    }))
    yield put(getScheduleSuccess(result.data))

  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
  }
}