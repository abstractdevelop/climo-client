import constants from '../constants'
import * as apiRoutes from '../constants/apiRoutes';
import { put, takeEvery} from 'redux-saga/effects'
import { getStatsFetched } from '../actions/stats'
import * as apiCall from '../api/req';

 

/* Stats */
export function * statsSaga() {
  yield takeEvery(constants.STATS, callStats)
}

function * callStats(action) {
  const {deviceId} = action.payload  
  try {
    const result = yield (apiCall.doRequest({
      method: 'GET',
      url: apiRoutes.STATS_URL.replace(apiRoutes.DEVICE_ID, deviceId),      
    }))    
    yield put(getStatsFetched(result.data))

  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
      // yield put(logInError(e))
      //yield callApiError(action, e)
  }
}