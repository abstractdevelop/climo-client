import constants from '../constants'
import * as apiRoutes from '../constants/apiRoutes';
import { put, takeEvery} from 'redux-saga/effects'
import { getProfileFetched, putUserFetched, putMeterLabelFetched } from '../actions/profile'
import * as apiCall from '../api/req';

 

/* Profile */
export function * getProfileSaga() {
  yield takeEvery(constants.GET_PROFILE, callGetProfile)
}

function * callGetProfile() {
  try {
    const result = yield (apiCall.doRequest({
      method: 'GET',
      url: apiRoutes.USER_PROFILE_URL,    
    }))    
    yield put(getProfileFetched(result.data))

  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
  }
}
/* Update User */
export function * putUserSaga() {
  yield takeEvery(constants.PUT_USER, callPutUser)
}

function * callPutUser(action) {
  
  const {_id, email, password, ...dataProfile  } = action.payload.user
  let user = { profile: dataProfile  }
  if(password){
    user.password = password
  }
  if(email){
    user.email = email
  }
  
  try {
    const result = yield (apiCall.doRequest({
      method: 'PUT',
      url: apiRoutes.UPDATE_USER_URL.replace(apiRoutes.USER_ID, _id),
      data: user
    }))    
    yield put(putUserFetched(result.data))

  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
  }
}
/* Update label meter */
export function * putMeterLabelSaga() {
  yield takeEvery(constants.PUT_METER_LABEL, callPutMeterLabel)
}

function * callPutMeterLabel(action) {
  const _id = action.payload.meter._id
  const label = action.payload.meter.label
  try {    
    const result = yield (apiCall.doRequest({
      method: 'PUT',
      url: apiRoutes.UPDATE_METER_LABEL_URL.replace(apiRoutes.METER_ID, _id),
      data: {label: label} 
    }))
    
    yield put(putMeterLabelFetched(result.data))
  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
  }
}


