import constants from '../constants'
import * as apiRoutes from '../constants/apiRoutes';
import { put, takeEvery} from 'redux-saga/effects'
import { loggedIn, logInError, recoverPasswordFetched } from '../actions/login';
import { userFetched } from '../actions/user'
import * as apiCall from '../api/req';
import {saveAuth, removeAuth} from '../helpers/auth'
 

/* LogIn */
export function * logInSaga() {
  yield takeEvery(constants.LOGIN, callLogin)
}

function * callLogin(action) {
  const {user} = action.payload  
  try {
    const result = yield (apiCall.doRequest({
      method: 'POST',
      url: apiRoutes.AUTH_USER_URL,
      data: user
    }))    
    removeAuth()   
    saveAuth(result.data)    
    const userResponse = {profile: result.data.profile, id: result.data._id, email: result.data.email }
    yield put(userFetched(userResponse))
    yield put(loggedIn(result.data))
  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
      yield put(logInError(e))
      //yield callApiError(action, e)
  }
}

/* RecoverPassword */
export function * recoverPasswordSaga() {
  yield takeEvery(constants.RECOVER_PASSWORD, callRecoverPassword)
}

function * callRecoverPassword(action) {
  const {email} = action.payload  
  try {
    const result = yield (apiCall.doRequest({
      method: 'POST',
      url: apiRoutes.FORGOT_PASS_URL,
      data: {email}
    }))        
      
    yield put(recoverPasswordFetched(result.data))
  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
      yield put(logInError(e))
      //yield callApiError(action, e)
  }
}
