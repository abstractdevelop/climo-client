import { fork, all } from 'redux-saga/effects'
import {logInSaga, recoverPasswordSaga} from './loginSaga'
import { statsSaga } from "./statsSaga";
import {getMetersSaga} from './meters'
import { getProfileSaga, putUserSaga, putMeterLabelSaga } from './profileSaga'
import {postUserSaga, postChangePassSaga } from './userSaga'
import {getCurrentWeatherSaga, fetchMetersForVariablesSaga} from './home';
import { sendCommandSaga } from './commandSagas';
import { getScheduleSaga, updateScheduleSaga } from './scheduleSaga';

export default function * root() {
  yield all([
    fork(logInSaga),
    fork(statsSaga),
    fork(getMetersSaga),
    fork(getProfileSaga),
    fork(postUserSaga),
    fork(getCurrentWeatherSaga),
    fork(putUserSaga),
    fork(putMeterLabelSaga),
    fork(fetchMetersForVariablesSaga),
    fork(sendCommandSaga),
    fork(getScheduleSaga),
    fork(updateScheduleSaga),
    fork(recoverPasswordSaga),
    fork(postChangePassSaga),

  ])
}