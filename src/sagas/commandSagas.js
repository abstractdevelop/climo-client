import constants from '../constants'
import { put, takeEvery} from 'redux-saga/effects'
import * as apiCall from '../api/req';
import * as apiRoutes from '../constants/apiRoutes';
import { sendCommandSuccess } from '../actions/commandActions';
 

/* COMMAND */
export function * sendCommandSaga() {
  yield takeEvery(constants.SEND_COMMAND, commandSaga)
}

function * commandSaga(action) {
    let command = action.payload.command
  try {
    const result = yield (apiCall.doRequest({
        method: 'POST',
        url: apiRoutes.SEND_ACTION,   
        data: command
      })) 
    yield put(sendCommandSuccess(result))
  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
  }
}
