import constants from '../constants'
import { put, takeEvery} from 'redux-saga/effects'
import * as weatherCall from '../api/weather';
import { currentWeatherFetched, fetchMetersForVariablesFetched } from '../actions/home';
import * as apiCall from '../api/req';
import * as apiRoutes from '../constants/apiRoutes';
 

/* LogIn */
export function * getCurrentWeatherSaga() {
  yield takeEvery(constants.GET_CURRENT_WEATHER, currentWeatherSaga)
}

function * currentWeatherSaga(action) {
  try {
    const result = yield (weatherCall.currentWeather(action.payload.lat, action.payload.lng))
    yield put(currentWeatherFetched(result.data))
  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
      //yield callApiError(action, e)
  }
}


export function * fetchMetersForVariablesSaga() {
  yield takeEvery(constants.GET_METERS_HOME, fetchMetersForVariables)
}

function * fetchMetersForVariables(action) {
  let meterId = action.payload.meterId
  try {
    const result = yield (apiCall.doRequest({
      method: 'GET',
      url: apiRoutes.METERS_VARIABLES.replace(apiRoutes.METER_SELECTED, meterId),      
    })) 
    yield put(fetchMetersForVariablesFetched(result.data))
  } catch(e) {
      console.log(`error api ${e.statusCode}`)
      console.log(e)
      //yield callApiError(action, e)
  }
}